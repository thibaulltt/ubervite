#include "../include/matrix.hpp"

#define MATRIX_EPSILON 1e-17

namespace core {

	/**
	 * =======================================================
	 * =======================================================
	 * =======================================================
	 * =======================================================
	 * ======================== mat3 =========================
	 * =======================================================
	 * =======================================================
	 * =======================================================
	 * =======================================================
	 */

	mat3::mat3() {
		_m[0].x = 1.0;
		_m[1].y = 1.0;
		_m[2].z = 1.0;
	}

	mat3::mat3(const double s) {
		_m[0].x = s;
		_m[1].y = s;
		_m[2].z = s;
	}

	mat3::mat3(const vec3& a, const vec3& b, const vec3& c) {
		_m[0] = a;
		_m[1] = b;
		_m[2] = c;
	}

	mat3::mat3(
		const double a, const double b, const double c,
		const double d, const double e, const double f,
		const double g, const double h, const double i) {
		_m[0] = vec3(a,b,c);
		_m[1] = vec3(d,e,f);
		_m[2] = vec3(g,h,i);
	}

	mat3::mat3(const mat3& m) {
		//if (*this != m) {
			_m[0] = m[0];
			_m[1] = m[1];
			_m[2] = m[2];
		//}
	}

	mat3::mat3(mat3&& m) {
		_m[0] = m._m[0];
		_m[1] = m._m[1];
		_m[2] = m._m[2];
	}

	mat3 mat3::transpose() const {
		return mat3(	this->_m[0][0], this->_m[1][0], this->_m[2][0],
				this->_m[0][1], this->_m[1][1], this->_m[2][1],
				this->_m[0][2], this->_m[1][2], this->_m[2][2]);
	}

	mat3& mat3::transpose_self() {
		double a = this->_m[0][0];
		double b = this->_m[0][1];
		double c = this->_m[0][2];
		double d = this->_m[1][0];
		double e = this->_m[1][1];
		double f = this->_m[1][2];
		double g = this->_m[2][0];
		double h = this->_m[2][1];
		double i = this->_m[2][2];
		this->_m[0] = vec3(a,d,g);
		this->_m[1] = vec3(b,e,h);
		this->_m[2] = vec3(c,f,i);
		return *this;
	}

	std::array<std::array<GLdouble, 3>, 3> mat3::to_array() const {
		std::array<std::array<GLdouble, 3>, 3> res;
		res[0][0] = static_cast<GLdouble>(this->_m[0][0]);
		res[0][1] = static_cast<GLdouble>(this->_m[0][1]);
		res[0][2] = static_cast<GLdouble>(this->_m[0][2]);
		res[1][0] = static_cast<GLdouble>(this->_m[1][0]);
		res[1][1] = static_cast<GLdouble>(this->_m[1][1]);
		res[1][2] = static_cast<GLdouble>(this->_m[1][2]);
		res[2][0] = static_cast<GLdouble>(this->_m[2][0]);
		res[2][1] = static_cast<GLdouble>(this->_m[2][1]);
		res[2][2] = static_cast<GLdouble>(this->_m[2][2]);
		return res;
	}

	double mat3::determinant() const{
		double a = this->_m[0][0];
		double b = this->_m[0][1];
		double c = this->_m[0][2];
		double d = this->_m[1][0];
		double e = this->_m[1][1];
		double f = this->_m[1][2];
		double g = this->_m[2][0];
		double h = this->_m[2][1];
		double i = this->_m[2][2];
		return a * (e * i - f * h) - b * (d * i - f * g) + c * (d * h - e * g);
	}

	mat3 mat3::normalize() const {
		return mat3(this->_m[0].normalize(), this->_m[1].normalize(), this->_m[2].normalize());
	}

	mat3& mat3::normalize_self() {
		this->_m[0].normalize_self();
		this->_m[1].normalize_self();
		this->_m[2].normalize_self();
		return *this;
	}

	mat3 mat3::inverse() const {
		double a = this->_m[0][0];
		double b = this->_m[0][1];
		double c = this->_m[0][2];
		double d = this->_m[1][0];
		double e = this->_m[1][1];
		double f = this->_m[1][2];
		double g = this->_m[2][0];
		double h = this->_m[2][1];
		double i = this->_m[2][2];
		double c0 = e * i - f * h;
		double c1 = f * g - d * i;
		double c2 = d * h - e * g;
		double idet = 1.0 / (a * c0 + b * c1 + c * c2);
		return mat3(	c0, c * h - b * i, b * f - c * e,
				c1, a * i - c * g, c * d - a * f,
				c2, b * g - a * h, a * e - b * d) * idet;
	}

	vec3& mat3::operator[](int i) {
		return this->_m[i];
	}

	const vec3& mat3::operator[](int i) const {
		return _m[i];
	}

	mat3 mat3::operator+ (const mat3& m) const {
		return mat3(
			this->_m[0] + m._m[0],
			this->_m[1] + m._m[1],
			this->_m[2] + m._m[2]
		);
	}

	mat3 mat3::operator- (const mat3& m) const {
		return mat3(
			this->_m[0] - m._m[0],
			this->_m[1] - m._m[1],
			this->_m[2] - m._m[2]
		);
	}

	mat3 mat3::operator* (const double s) const {
		return mat3(
			this->_m[0] * s,
			this->_m[1] * s,
			this->_m[2] * s
		);
	}

	mat3 mat3::operator/ (const double s) const {
		return mat3(
			this->_m[0] / s,
			this->_m[1] / s,
			this->_m[2] / s
		);
	}

	mat3 mat3::operator* (const mat3& m) const {
		mat3 res{0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};

		for ( int i = 0; i < 3; ++i ) {
			for ( int j = 0; j < 3; ++j ) {
				for ( int k = 0; k < 3; ++k ) {
					res[i][j] += this->_m[i][k] * m[k][j];
				}
			}
		}

		return res;
	}

	mat3& mat3::operator+=(const mat3& m) {
		this->_m[0] += m._m[0];
		this->_m[1] += m._m[1];
		this->_m[2] += m._m[2];
		return *this;
	}

	mat3& mat3::operator-=(const mat3& m) {
		this->_m[0] -= m._m[0];
		this->_m[1] -= m._m[1];
		this->_m[2] -= m._m[2];
		return *this;
	}

	mat3& mat3::operator*=(const double s) {
		this->_m[0] *= s;
		this->_m[1] *= s;
		this->_m[2] *= s;
		return *this;
	}

	mat3& mat3::operator/=(const double s) {
		this->_m[0] /= s;
		this->_m[1] /= s;
		this->_m[2] /= s;
		return *this;
	}

	mat3& mat3::operator= (const mat3& m) {
		this->_m[0] = m._m[0];
		this->_m[1] = m._m[1];
		this->_m[2] = m._m[2];
		return *this;
	}

	vec3 mat3::operator*=(const vec3& v) const {
		return vec3(this->_m[0][0]*v.x + this->_m[0][1]*v.y + this->_m[0][2]*v.z,
			this->_m[1][0]*v.x + this->_m[1][1]*v.y + this->_m[1][2]*v.z,
			this->_m[2][0]*v.x + this->_m[2][1]*v.y + this->_m[2][2]*v.z );
	}

	vec3 mat3::operator* (const vec3& v) const {
		return vec3(this->_m[0][0]*v.x + this->_m[0][1]*v.y + this->_m[0][2]*v.z,
			this->_m[1][0]*v.x + this->_m[1][1]*v.y + this->_m[1][2]*v.z,
			this->_m[2][0]*v.x + this->_m[2][1]*v.y + this->_m[2][2]*v.z );
	}

	/**
	 * =======================================================
	 * =======================================================
	 * =======================================================
	 * =======================================================
	 * ======================== mat4 =========================
	 * =======================================================
	 * =======================================================
	 * =======================================================
	 * =======================================================
	 */

	mat4::mat4() {
		_m[0] = vec4();
		_m[1] = vec4();
		_m[2] = vec4();
		_m[3] = vec4();
		_m[0].x = 1.0;
		_m[1].y = 1.0;
		_m[2].z = 1.0;
		_m[3].w = 1.0;
	}

	mat4::mat4(const double s) {
		_m[0] = vec4();
		_m[1] = vec4();
		_m[2] = vec4();
		_m[3] = vec4();
		_m[0].x = s;
		_m[1].y = s;
		_m[2].z = s;
		_m[3].w = s;
	}

	mat4::mat4(const vec4& a, const vec4& b, const vec4& c, const vec4& d) {
		_m[0] = a;
		_m[1] = b;
		_m[2] = c;
		_m[3] = d;
	}

	mat4::mat4(
		const double a, const double b, const double c, const double d,
		const double e, const double f, const double g, const double h,
		const double i, const double j, const double k, const double l,
		const double m, const double n, const double p, const double q) {
		_m[0] = vec4(a,b,c,d);
		_m[1] = vec4(e,f,g,h);
		_m[2] = vec4(i,j,k,l);
		_m[3] = vec4(m,n,p,q);
	}

	mat4::mat4(const aiMatrix4x4 m) {
		this->_m[0][0] = static_cast<double>(m[0][0]);
		this->_m[0][1] = static_cast<double>(m[0][1]);
		this->_m[0][2] = static_cast<double>(m[0][2]);
		this->_m[0][3] = static_cast<double>(m[0][3]);
		this->_m[1][0] = static_cast<double>(m[1][0]);
		this->_m[1][1] = static_cast<double>(m[1][1]);
		this->_m[1][2] = static_cast<double>(m[1][2]);
		this->_m[1][3] = static_cast<double>(m[1][3]);
		this->_m[2][0] = static_cast<double>(m[2][0]);
		this->_m[2][1] = static_cast<double>(m[2][1]);
		this->_m[2][2] = static_cast<double>(m[2][2]);
		this->_m[2][3] = static_cast<double>(m[2][3]);
		this->_m[3][0] = static_cast<double>(m[3][0]);
		this->_m[3][1] = static_cast<double>(m[3][1]);
		this->_m[3][2] = static_cast<double>(m[3][2]);
		this->_m[3][3] = static_cast<double>(m[3][3]);
	}

	mat4::mat4(glm::mat4x4 mat) {
		this->_m[0][0] = static_cast<double>(mat[0][0]);
		this->_m[0][1] = static_cast<double>(mat[0][1]);
		this->_m[0][2] = static_cast<double>(mat[0][2]);
		this->_m[0][3] = static_cast<double>(mat[0][3]);
		this->_m[1][0] = static_cast<double>(mat[1][0]);
		this->_m[1][1] = static_cast<double>(mat[1][1]);
		this->_m[1][2] = static_cast<double>(mat[1][2]);
		this->_m[1][3] = static_cast<double>(mat[1][3]);
		this->_m[2][0] = static_cast<double>(mat[2][0]);
		this->_m[2][1] = static_cast<double>(mat[2][1]);
		this->_m[2][2] = static_cast<double>(mat[2][2]);
		this->_m[2][3] = static_cast<double>(mat[2][3]);
		this->_m[3][0] = static_cast<double>(mat[3][0]);
		this->_m[3][1] = static_cast<double>(mat[3][1]);
		this->_m[3][2] = static_cast<double>(mat[3][2]);
		this->_m[3][3] = static_cast<double>(mat[3][3]);
		this->transpose_self();
	}

	mat4::mat4(const mat3& m) {
		_m[0] = vec4(m[0]);
		_m[1] = vec4(m[1]);
		_m[2] = vec4(m[2]);
		_m[3] = vec4(0.0,0.0,0.0,1.0);
	}

	mat4::mat4(const mat4& m) {
		//if (*this != m) {
			_m[0] = m[0];
			_m[1] = m[1];
			_m[2] = m[2];
			_m[3] = m[3];
		//}
	}

	mat4::mat4(mat4&& m) {
		_m[0] = m._m[0];
		_m[1] = m._m[1];
		_m[2] = m._m[2];
		_m[3] = m._m[3];
	}

	std::array<std::array<GLdouble, 4>, 4> mat4::to_array() const {
		std::array<std::array<GLdouble, 4>, 4> res;
		res[0][0] = static_cast<GLdouble>(this->_m[0][0]);
		res[0][1] = static_cast<GLdouble>(this->_m[0][1]);
		res[0][2] = static_cast<GLdouble>(this->_m[0][2]);
		res[0][3] = static_cast<GLdouble>(this->_m[0][3]);
		res[1][0] = static_cast<GLdouble>(this->_m[1][0]);
		res[1][1] = static_cast<GLdouble>(this->_m[1][1]);
		res[1][2] = static_cast<GLdouble>(this->_m[1][2]);
		res[1][3] = static_cast<GLdouble>(this->_m[1][3]);
		res[2][0] = static_cast<GLdouble>(this->_m[2][0]);
		res[2][1] = static_cast<GLdouble>(this->_m[2][1]);
		res[2][2] = static_cast<GLdouble>(this->_m[2][2]);
		res[2][3] = static_cast<GLdouble>(this->_m[2][3]);
		res[3][0] = static_cast<GLdouble>(this->_m[3][0]);
		res[3][1] = static_cast<GLdouble>(this->_m[3][1]);
		res[3][2] = static_cast<GLdouble>(this->_m[3][2]);
		res[3][3] = static_cast<GLdouble>(this->_m[3][3]);
		return res;
	}

	mat4 mat4::transpose() const {
		return mat4(	this->_m[0][0], this->_m[1][0], this->_m[2][0], this->_m[3][0],
				this->_m[0][1], this->_m[1][1], this->_m[2][1], this->_m[3][1],
				this->_m[0][2], this->_m[1][2], this->_m[2][2], this->_m[3][2],
				this->_m[0][3], this->_m[1][3], this->_m[2][3], this->_m[3][3]);
	}

	mat4& mat4::transpose_self() {
		double a = this->_m[0][0];
		double b = this->_m[0][1];
		double c = this->_m[0][2];
		double d = this->_m[0][3];
		double e = this->_m[1][0];
		double f = this->_m[1][1];
		double g = this->_m[1][2];
		double h = this->_m[1][3];
		double i = this->_m[2][0];
		double j = this->_m[2][1];
		double k = this->_m[2][2];
		double l = this->_m[2][3];
		double m = this->_m[3][0];
		double n = this->_m[3][1];
		double o = this->_m[3][2];
		double p = this->_m[3][3];
		this->_m[0] = vec4(a,e,i,m);
		this->_m[1] = vec4(b,f,j,n);
		this->_m[2] = vec4(c,g,k,o);
		this->_m[3] = vec4(d,h,l,p);
		return *this;
	}

	mat4 mat4::fast_inverse() const {
		mat3 a = mat3(	this->_m[0][0], this->_m[0][1], this->_m[0][2],
				this->_m[1][0], this->_m[1][1], this->_m[1][2],
				this->_m[2][0], this->_m[2][2], this->_m[2][2]);
		vec3 b = vec3(this->_m[0][3], this->_m[1][3], this->_m[2][3]);
		mat3 x = a.inverse();
		vec3 y = x * b;
		return mat4(	x._m[0][0], x._m[0][1], x._m[0][2], -y.x,
				x._m[1][0], x._m[1][1], x._m[1][2], -y.y,
				x._m[2][0], x._m[2][1], x._m[2][2], -y.z,
				0.0, 0.0, 0.0, 1.0);
	}

	mat4 mat4::inverse() const {
		return this->adjoint_matrix() * (1.0 / this->determinant());
	}

	double mat4::minor(const int r0, const int r1, const int r2, const int c0, const int c1, const int c2) const {
		return mat3(	this->_m[r0][c0], this->_m[r0][c1], this->_m[r0][c2],
				this->_m[r1][c0], this->_m[r1][c1], this->_m[r1][c2],
				this->_m[r2][c0], this->_m[r2][c1], this->_m[r2][c2]).determinant();
	}

	mat4 mat4::adjoint_matrix() const {
		return mat4(
			 this->minor(1,2,3,1,2,3), -this->minor(0,2,3,1,2,3),  this->minor(0,1,3,1,2,3), -this->minor(0,1,2,1,2,3),
			-this->minor(1,2,3,0,2,3),  this->minor(0,2,3,0,2,3), -this->minor(0,1,3,0,2,3),  this->minor(0,1,2,0,2,3),
			 this->minor(1,2,3,0,1,3), -this->minor(0,2,3,0,1,3),  this->minor(0,1,3,0,1,3), -this->minor(0,1,2,0,1,3),
			-this->minor(1,2,3,0,1,2),  this->minor(0,2,3,0,1,2), -this->minor(0,1,3,0,1,2),  this->minor(0,1,2,0,1,2)
		);
	}

	bool mat4::is_inversible() const {
		return((this->_m[3] - vec4(0.,0.,0.,1.)).length() < MATRIX_EPSILON) &&
			this->_m[3][3] > (1.0 - MATRIX_EPSILON) && this->_m[3][3] < (1.0 + MATRIX_EPSILON);
	}

	double mat4::determinant() const {
		return	this->_m[0][0] * this->minor(1,2,3,1,2,3) -
			this->_m[0][1] * this->minor(1,2,3,0,2,3) +
			this->_m[0][2] * this->minor(1,2,3,0,1,3) -
			this->_m[0][1] * this->minor(1,2,3,0,1,2);
	}

	vec4& mat4::operator[](int i) {
		return this->_m[i];
	}

	const vec4& mat4::operator[](int i) const {
		return _m[i];
	}

	mat4& mat4::operator= (const mat4& m) {
		this->_m[0] = m[0];
		this->_m[1] = m[1];
		this->_m[2] = m[2];
		this->_m[3] = m[3];
		return *this;
	}

	mat4 mat4::operator+ (const mat4& m) const {
		return mat4(
			this->_m[0] + m._m[0],
			this->_m[1] + m._m[1],
			this->_m[2] + m._m[2],
			this->_m[3] + m._m[3]
		);
	}

	mat4 mat4::operator- (const mat4& m) const {
		return mat4(
			this->_m[0] - m._m[0],
			this->_m[1] - m._m[1],
			this->_m[2] - m._m[2],
			this->_m[3] - m._m[3]
		);
	}

	mat4 mat4::operator* (const double s) const {
		return mat4(
			this->_m[0] * s,
			this->_m[1] * s,
			this->_m[2] * s,
			this->_m[3] * s
		);
	}

	mat4 mat4::operator/ (const double s) const {
		return mat4(
			this->_m[0] / s,
			this->_m[1] / s,
			this->_m[2] / s,
			this->_m[3] / s
		);
	}

	mat4 mat4::operator* (const mat4& m) const {
		mat4 res{0.0};

		for ( int i = 0; i < 4; ++i ) {
			for ( int j = 0; j < 4; ++j ) {
				for ( int k = 0; k < 4; ++k ) {
					res[i][j] += this->_m[i][k] * m[k][j];
				}
			}
		}

		return res;
	}

	vec4 mat4::operator* (const vec4& v) const {
		double vx = this->_m[0][0] * v.x + this->_m[0][1] * v.y + this->_m[0][2] * v.z + this->_m[0][3] * v.w;
		double vy = this->_m[1][0] * v.x + this->_m[1][1] * v.y + this->_m[1][2] * v.z + this->_m[1][3] * v.w;
		double vz = this->_m[2][0] * v.x + this->_m[2][1] * v.y + this->_m[2][2] * v.z + this->_m[2][3] * v.w;
		double vw = this->_m[3][0] * v.x + this->_m[3][1] * v.y + this->_m[3][2] * v.z + this->_m[3][3] * v.w;
		return vec4(vx,vy,vz,vw);
	}

	mat4& mat4::operator+=(const mat4& m) {
		this->_m[0] += m._m[0];
		this->_m[1] += m._m[1];
		this->_m[2] += m._m[2];
		this->_m[3] += m._m[3];
		return *this;
	}

	mat4& mat4::operator-=(const mat4& m) {
		this->_m[0] -= m._m[0];
		this->_m[1] -= m._m[1];
		this->_m[2] -= m._m[2];
		this->_m[3] -= m._m[3];
		return *this;
	}

	mat4& mat4::operator*=(const double s) {
		this->_m[0] *= s;
		this->_m[1] *= s;
		this->_m[2] *= s;
		this->_m[3] *= s;
		return *this;
	}

	mat4& mat4::operator/=(const double s) {
		this->_m[0] /= s;
		this->_m[1] /= s;
		this->_m[2] /= s;
		this->_m[3] /= s;
		return *this;
	}

	vec4 mat4::operator*=(const vec4& v) const {
		return vec4(_m[0][0]*v.x + _m[0][1]*v.y + _m[0][2]*v.z + _m[0][3]*v.w,
			_m[1][0]*v.x + _m[1][1]*v.y + _m[1][2]*v.z + _m[1][3]*v.w,
			_m[2][0]*v.x + _m[2][1]*v.y + _m[2][2]*v.z + _m[2][3]*v.w,
			_m[3][0]*v.x + _m[3][1]*v.y + _m[3][2]*v.z + _m[3][3]*v.w );
	}

	/**
	 * =======================================================
	 * =======================================================
	 * =======================================================
	 * =======================================================
	 * ==================== operations =======================
	 * =======================================================
	 * =======================================================
	 * =======================================================
	 * =======================================================
	 */

	mat3 freeRotate3D(const double theta, const vec3& u) {
		double alpha = DegToRad * theta;

		mat3 output;

		double ux2 = pow(u.x, 2);
		double uy2 = pow(u.y, 2);
		double uz2 = pow(u.z, 2);

		/**
		 * Tiré de la formule matricielle de Rodrigues pour la rotation selon un vecteur U
		 */

		output[0][0] = ux2 + (1-ux2)*cos(alpha);
		output[0][1] = u.x * u.y * (1-cos(alpha)) - u.z * sin(alpha);
		output[0][2] = u.x * u.z * (1-cos(alpha)) + u.y * sin(alpha);

		output[1][0] = u.x * u.y * (1-cos(alpha)) + u.z * sin(alpha);
		output[1][1] = uy2 + (1-uy2)*cos(alpha);
		output[1][2] = u.y * u.z * (1-cos(alpha)) - u.x * sin(alpha);

		output[2][0] = u.x * u.z * (1-cos(alpha)) - u.y * sin(alpha);
		output[2][1] = u.y * u.z * (1-cos(alpha)) - u.x * sin(alpha);
		output[2][2] = uz2 + (1-uz2)*cos(alpha);

		return output;
	}

	mat3 customRotationMatrixFromAxis(const double t, const vec3& v) {
		mat3 output;

		mat3 vectorialFromAxis{0.0, -v[2], v[1], v[2], 0.0, -v[0], -v[1], v[0], 0};

		return output + vectorialFromAxis * std::sin(t) + vectorialFromAxis * vectorialFromAxis * (1.0 - std::cos(t));
	}

	inline mat4 translate(const double x, const double y, const double z) {
		mat4 output;

		output[0][3] = x;
		output[1][3] = y;
		output[2][3] = z;

		return output;
	}

	inline mat4 translate(const vec3& v) {
		return translate(v.x, v.y, v.z);
	}

	inline mat4 translate(const vec4& v) {
		return translate(v.x, v.y, v.z);
	}

	inline mat4 scale(const double x, const double y, const double z) {
		mat4 output{0.0};

		output[0][0] = x;
		output[1][1] = y;
		output[2][2] = z;

		return output;
	}

	inline mat4 scale(const vec3& v) {
		return scale(v.x,v.y,v.z);
	}

	mat4 generate_lookat_matrix(vec4 from, vec4 to) {
		core::vec4 forward = (to-from).normalize_self();
		core::vec4 right = forward.cross(core::vec4(.0,1.0,.0,.0));
		core::vec4 up = forward.cross(right);

		return core::mat4(
			right.x, up.x, forward.x, from.x,
			right.y, up.y, forward.y, from.y,
			right.z, up.z, forward.z, from.z,
			.0, .0, .0, 1.0
		);
	}

	mat4 generate_lookat_matrix(vec4 from, vec4 to, vec4 up) {
		core::vec4 forward = (to-from).normalize_self();
		core::vec4 right = forward.cross(core::vec4(.0,1.0,.0,.0));

		return core::mat4(
			right.x, up.x, forward.x, from.x,
			right.y, up.y, forward.y, from.y,
			right.z, up.z, forward.z, from.z,
			.0, .0, .0, 1.0
		);
	}

	mat4 generate_projection_matrix(double bottom, double top, double left, double right, double near, double far) {
		mat4 res = mat4();

		near = -near;
		far = -far;

		double x_denom = (right - left);
		double y_denom = (top - bottom);
		double z_denom = (far - near);

		res[0][0] = (2.0 * near) / x_denom;
		res[0][2] = (right + left) / x_denom;
		res[1][1] = (2.0 * near) / y_denom;
		res[1][2] = (top + bottom) / y_denom;
		res[2][2] = -(far + near) / z_denom;
		res[2][3] = -2.0 * far * near / z_denom;
		res[3][2] = -1.0;

		return res.transpose_self();
	}

}
