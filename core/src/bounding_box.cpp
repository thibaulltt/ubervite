#include "../include/bounding_box.hpp"

core::bounding_box::bounding_box() {
	this->min = core::vec4(BB_COORD_MAX, BB_COORD_MAX, BB_COORD_MAX, BB_COORD_MAX);
	this->max = core::vec4(BB_COORD_MIN, BB_COORD_MIN, BB_COORD_MIN, BB_COORD_MIN);
}

core::bounding_box::bounding_box(core::vec4 p) {
	this->min = p;
	this->max = p;
}

core::bounding_box::bounding_box(const bounding_box& b) {
	this->min = b.min;
	this->max = b.max;
}

core::vec4 core::bounding_box::operator[](size_t i) const {
	return (i == 0) ? this->min : this->max;
}

core::vec4 core::bounding_box::corner(size_t c) const {
	assert(c < 8);

	return core::vec4((*this)[(c&1)].x, (*this)[(c&2) ? 1:0].y, (*this)[(c&4) ? 1:0].z, 1.);
}

void core::bounding_box::extend(core::vec4 to_add) {
	// Check for max coords :
	if (to_add.x > this->max.x) { this->max.x = to_add.x; }
	if (to_add.y > this->max.y) { this->max.y = to_add.y; }
	if (to_add.z > this->max.z) { this->max.z = to_add.z; }

	// Check for min coords :
	if (to_add.x < this->min.x) { this->min.x = to_add.x; }
	if (to_add.y < this->min.y) { this->min.y = to_add.y; }
	if (to_add.z < this->min.z) { this->min.z = to_add.z; }
}

core::vec4 core::bounding_box::center() const {
	return this->min + (this->max - this->min) * .5;
}

bool core::bounding_box::is_inside(core::vec4 to_test) const {
	return  to_test.x > this->min.x && to_test.x < this->max.x &&
	        to_test.y > this->min.y && to_test.y < this->max.y &&
	        to_test.z > this->min.z && to_test.z < this->max.z;
}
