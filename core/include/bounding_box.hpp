#ifndef _CORE_INCLUDE_BOUNDING_BOX_HPP_
#define _CORE_INCLUDE_BOUNDING_BOX_HPP_

#include "./vector.hpp"
#include "./matrix.hpp"
#include "./transform.hpp"

// TODO : Extend the class !

#include <limits>

#define BB_COORD_MIN std::numeric_limits<double>::min()
#define BB_COORD_MAX std::numeric_limits<double>::max()

namespace core {

	/**
	 * @brief The bounding_box class represents a bounding box.
	 * @details The bounding_box class represents a bounding box. Since it is rattached to a scene_object,
	 * it will be an axis-aligned bounding box (aka AABB). There is a convenience function to convert it
	 * to world-space, as well as a function to convert it to (another-)object-space. It allows for easy
	 * checking of collisions in 3D space.
	 * @note A drawable version of this class will be available at a later date.
	 */
	class bounding_box {
		protected:
			/**
			 * @brief Point at the minimum coordinates of the bounding box
			 */
			core::vec4 min;
			/**
			 * @brief Point at the maximum coordinates of the bounding box
			 */
			core::vec4 max;
		public:
			/**
			 * @brief Create an empty bounding box, with no position in space.
			 */
			bounding_box();
			/**
			 * @brief Bounding box created at a point in space
			 * @param p the point to create the bounding box around
			 */
			bounding_box(core::vec4 p);
			/**
			 * @brief Copy constructor from another bounding box
			 * @param b The bounding box to copy data from
			 */
			bounding_box(const bounding_box& b);
			/**
			 * @brief Get a read-only access to the point at the minimum coordinates.
			 * @return A const reference to min
			 */
			const core::vec4& get_min() const { return this->min; }
			/**
			 * @brief Get a read-only access to the point at the maximum coordinates.
			 * @return A const reference to max
			 */
			const core::vec4& get_max() const { return this->max; }
			/**
			 * @brief Get the position of a corner of the bounding box
			 * // TODO : add details about the corner order
			 * @param c The corner to get
			 * @return The position of corner `c`
			 */
			core::vec4 corner(size_t c) const;
			/**
			 * @brief Extend the bounding box if the point given is outside of it. Otherwise, do nothing.
			 * @param to_add The point to add to the bounding box
			 */
			void extend(core::vec4 to_add);
			/**
			 * @brief Checks if a point is inside the bounding box or not
			 * @param to_test
			 * @return True is the point is inside, false if not
			 */
			bool is_inside(core::vec4 to_test) const;
			/**
			 * @brief Checks if the bounding boxes overlap
			 * @param to_test The bounding box to check
			 * @return True if they overlap, false if they dont
			 */
			bool overlaps(const core::bounding_box& to_test) const; // NOT IMPLEMENTED
			/**
			 * @brief Returns a point at the center of the bounding box.
			 * @return The center of the bounding box
			 */
			core::vec4 center() const;
			/**
			 * @brief Creates a bounding box equal to this one, but expressed in world coordinates instead of object coordinates
			 * @param t the transform to get the world-space coordinates of the bounding box
			 * @return The world-space copy of this bounding box
			 */
			core::bounding_box to_world_coordinates(const core::mat4& t) const; // NOT IMPLEMENTED
			/**
			 * @brief Creates a bounding box equal to this one, but expressed in the transform's coordinates instead of object coordinates
			 * @param t
			 * @return
			 */
			core::bounding_box to_object_coordinates(const core::mat4& t) const; // NOT IMPLEMENTED
		private:
			/**
			 * @brief Access to one of the points
			 * @param i
			 * @return
			 */
			core::vec4 operator[](size_t i) const;
	};

}

#endif // _CORE_INCLUDE_BOUNDING_BOX_HPP_
