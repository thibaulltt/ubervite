/**==========================================
 * FILE : matrix.hpp
 * AUTH : Thibault de VILLÈLE
 * DATE : 18/10/2019
 * DESC : Contient les définitions pour les
 *        matrices utilisées pour les transformations
 *        géométriques en 3 et 4 dimensions.
 * ==========================================
 */

#ifndef _CORE_TRANSFORM_HPP_
#define _CORE_TRANSFORM_HPP_

#include "vector.hpp"
#include <array>

#include <cmath>
#include <assimp/matrix4x4.h>
#include <assimp/scene.h>

#include <glm/detail/compute_common.hpp>
#include <glm/gtc/matrix_transform.hpp>

#ifndef __gl_h_
#include <GL/gl.h>
#endif

namespace core {

	// Classe mat3 {{{
	/**
	 * @brief The mat3 class is a class designed to hold 3x3 matrices.
	 * @details This class represents a 3x3 matrix, in ROW-MAJOR order.
	 * If we want to pass it on to OpenGL, which is COLUMN-MAJOR, we
	 * will need to transpose it first.
	 * @warning Those matrices are row-major. Not column-major as OpenGL would like them to be.
	 */
	class mat3 {
		public:
			vec3 _m[3];
		public:
			// Constructeurs pour des matrices 3x3 {{{
			/**
			 * @brief Constructeur par défaut. Construit une matrice identitaire.
			 */
			mat3();
			/**
			 * @brief Constructeur de matrices identitaires scalaires.
			 * @details Multiplie une matrice identitaire par le scalaire donné.
			 */
			mat3(const double);
			/**
			 * @brief Construit entièrement une matrice grace aux vecteurs fournis.
			 */
			mat3(const vec3&, const vec3&, const vec3&);
			/**
			 * @brief Construit une matrice en spécifiant tous les champs de celle-ci.
			 */
			mat3(	const double, const double, const double,
				const double, const double, const double,
				const double, const double, const double);
			/**
			 * @brief Constructeur par copie.
			 */
			mat3(const mat3&);
			/**
			 * @brief Constructeur par copie.
			 */
			mat3(mat3&&);
			// }}}

			// Opérations géométriques et numériques sur la matrice {{{
			/**
			 * @brief Transposes a copy of the current matrix.
			 * @return A transposed copy of the matrix
			 */
			inline mat3 transpose() const;
			/**
			 * @brief Transposes the current matrix.
			 * @return A reference to `this`
			 */
			inline mat3& transpose_self();
			/**
			 * @brief Computes the determinant of the matrix
			 * @return The determinant of the matrix
			 */
			inline double determinant() const;
			/**
			 * @brief Returns a normalized copy of the matrix
			 * @return A normalized copy of the current matrix.
			 */
			inline mat3 normalize() const;
			/**
			 * @brief Normalizes the current matrix
			 * @return A reference to `this`
			 */
			inline mat3& normalize_self();
			/**
			 * @brief Inverses a copy of the current matrix
			 * @return An inverse matrix for `this`
			 */
			mat3 inverse() const;
			// }}}

			std::array<std::array<GLdouble, 3>, 3> to_array() const;
			// Accesseurs aux composants de la matrice {{{
			/**
			 * @brief Accès tabulaire à la matrice
			 */
			vec3& operator[](int i);
			/**
			 * @brief Accès tabulaire en LECTURE à la matrice
			 */
			const vec3& operator[](int i) const;
			// }}}

			// Opérateurs sur la matrice : +,-,*,/,+=,-=,*=,/= ... {{{
			/**
			 * @brief Opération d'addition de matrices. Retourne une nouvelle matrice.
			 */
			mat3 operator+ (const mat3&) const;
			/**
			 * @brief Opération de soustraction de matrices. Retourne une nouvelle matrice.
			 */
			mat3 operator- (const mat3&) const;
			/**
			 * @brief Multiplication de la matrice par un scalaire. Retourne une nouvelle matrice.
			 */
			mat3 operator* (const double) const;
			/**
			 * @brief Division de la matrice par un scalaire. Retourne une nouvelle matrice.
			 */
			mat3 operator/ (const double) const;
			/**
			 * @brief Multiplication de matrices. Retourne une nouvelle matrice.
			 */
			mat3 operator* (const mat3&) const;
			/**
			 * @brief Addition de matrice dans l'objet courant.
			 */
			mat3& operator+=(const mat3&);
			/**
			 * @brief Soustraction de matrices dans l'objet courant.
			 */
			mat3& operator-=(const mat3&);
			/**
			 * @brief Multiplication de la matrice courante par un scalaire.
			 */
			mat3& operator*=(const double);
			/**
			 * @brief Division de la matrice courante par un scalaire.
			 */
			mat3& operator/=(const double);
			/**
			 * @brief Opérateur d'assignation de matrices
			 */
			mat3& operator= (const mat3&);
			/**
			 * @brief Multiplie la matrice par un vecteur. Retourne un nouveau vecteur.
			 */
			vec3 operator*=(const vec3&) const;
			/**
			 * @brief Multiplie la matrice par un vecteur. Retourne un nouveau vecteur.
			 */
			vec3 operator* (const vec3&) const;
			// }}}

			// Sortie pour affichage et entrée pour insertion {{{
			/**
			 * @brief Opérateur d'extraction
			 */
			friend std::ostream& operator<<(std::ostream& os, const mat3& m) {
				return os << std::endl << m._m[0] << std::endl << m._m[1] << std::endl << m._m[2] << std::endl;
			}
			/**
			 * @brief Opérateur d'insertion
			 */
			friend std::istream& operator>>(std::istream& is, mat3& m) {
				return is >> m._m[0] >> m._m[1] >> m._m[2];
			}
			// }}}
	};
	// }}}

	// Classe mat4 {{{
	class mat4 {
		public:
			vec4 _m[4];
		public:
			// Constructeurs pour des matrices 4x4 {{{
			/**
			 * Constructeur par défaut. Construit une matrice identitaire.
			 */
			mat4();
			/**
			 * Constructeur de matrices identitaires scalaires. Multiplie
			 * une matrice identitaire par le scalaire donné.
			 */
			mat4(const double);
			/**
			 * Construit entièrement une matrice grace aux vecteurs fournis.
			 */
			mat4(const vec4&, const vec4&, const vec4&, const vec4&);
			/**
			 * Construit une matrice en spécifiant tous les champs de celle-ci.
			 */
			mat4(	const double, const double, const double, const double,
				const double, const double, const double, const double,
				const double, const double, const double, const double,
				const double, const double, const double, const double);
			mat4(const aiMatrix4x4);
			mat4(glm::mat4x4 mat);
			/**
			 * Constructeur remplissant une matrice 4 par les éléments d'une matrice 3
			 */
			mat4(const mat3&);
			/**
			 * Constructeur par copie.
			 */
			mat4(const mat4&);
			/**
			 * Constructeur par copie.
			 */
			mat4(mat4&&);
			// }}}

			/**
			 * @brief Transposes the matrix.
			 * @return A transposed copy of the matrix.
			 */
			mat4 transpose() const;

			/**
			 * @brief Transposes the current matrix.
			 * @return A reference to `this`
			 */
			mat4& transpose_self();

			/**
			 * @brief Returns a pseudo inverse of the current matrix.
			 * @details Only works if the matrix represents an affine
			 * transformation (rotation, translation, shear, scaling).
			 * In other words, don't apply this on the projection
			 * matrix, or any non-orthogonal matrix.
			 * @warning Undefined behaviour might happen if the matrix
			 * is not representing an affine transformation (the last
			 * row isn't [0,0,0,1]).
			 * @return A pseudo-inverse of the current matrix.
			 * @see inverse()
			 */
			mat4 fast_inverse() const;

			/**
			 * @brief Fully inverses the current matrix, regardless of it's type.
			 * @return The inverse of a copy of this matrix.
			 * @see fast_inverse()
			 */
			mat4 inverse() const;

			/**
			 * @brief Checks if the matrix is inversible.
			 * @warning Only checks if the vector of the bottom row is equal to [0,0,0,1]
			 * @return True if the matrix is inversible, false if not.
			 */
			bool is_inversible() const;

			/**
			 * @brief Computes the determinant of the current matrix
			 * @return The determinant of the current matrix
			 */
			double determinant() const;

			std::array<std::array<GLdouble, 4>, 4> to_array() const;

			// Accesseurs à la matrice {{{
			/**
			 * Accès tabulaire à la matrice
			 */
			vec4& operator[](int i);
			/**
			 * Accès tabulaire en LECTURE à la matrice
			 */
			const vec4& operator[](int i) const;
			// }}}

			double* to_array_double() const {
				double* m = new double[16];
				m[ 0] = this->_m[0][0];
				m[ 1] = this->_m[0][1];
				m[ 2] = this->_m[0][2];
				m[ 3] = this->_m[0][3];
				m[ 4] = this->_m[1][0];
				m[ 5] = this->_m[1][1];
				m[ 6] = this->_m[1][2];
				m[ 7] = this->_m[1][3];
				m[ 8] = this->_m[2][0];
				m[ 9] = this->_m[2][1];
				m[10] = this->_m[2][2];
				m[11] = this->_m[2][3];
				m[12] = this->_m[3][0];
				m[13] = this->_m[3][1];
				m[14] = this->_m[3][2];
				m[15] = this->_m[3][3];

				return m;
			}

			float* to_array_float() const {
				float* m = new float[16];
				m[ 0] = static_cast<float>(this->_m[0][0]);
				m[ 1] = static_cast<float>(this->_m[0][1]);
				m[ 2] = static_cast<float>(this->_m[0][2]);
				m[ 3] = static_cast<float>(this->_m[0][3]);
				m[ 4] = static_cast<float>(this->_m[1][0]);
				m[ 5] = static_cast<float>(this->_m[1][1]);
				m[ 6] = static_cast<float>(this->_m[1][2]);
				m[ 7] = static_cast<float>(this->_m[1][3]);
				m[ 8] = static_cast<float>(this->_m[2][0]);
				m[ 9] = static_cast<float>(this->_m[2][1]);
				m[10] = static_cast<float>(this->_m[2][2]);
				m[11] = static_cast<float>(this->_m[2][3]);
				m[12] = static_cast<float>(this->_m[3][0]);
				m[13] = static_cast<float>(this->_m[3][1]);
				m[14] = static_cast<float>(this->_m[3][2]);
				m[15] = static_cast<float>(this->_m[3][3]);

				return m;
			}

			// Opérateurs {{{
			/**
			 * Opérateur d'affectation de matrices
			 */
			mat4& operator= (const mat4&);
			/**
			 * Opération d'addition de matrices. Retourne une nouvelle matrice.
			 */
			mat4 operator+ (const mat4&) const;
			/**
			 * Opération de soustraction de matrices. Retourne une nouvelle matrice.
			 */
			mat4 operator- (const mat4&) const;
			/**
			 * Multiplication de la matrice par un scalaire. Retourne une nouvelle matrice.
			 */
			mat4 operator* (const double) const;
			/**
			 * Division de la matrice par un scalaire. Retourne une nouvelle matrice.
			 */
			mat4 operator/ (const double) const;
			/**
			 * Multiplication de matrices. Retourne une nouvelle matrice.
			 */
			mat4 operator* (const mat4&) const;
			/**
			 * @brief Multiplication of a vector by a matrix. Returns a new matrix.
			 * @return
			 */
			vec4 operator* (const vec4&) const;
			/**
			 * @brief Reversed operator for vector-matrix multiplication
			 * @param v The vector to multiply
			 * @param m The matrix to use for the multiplication
			 * @return The operation of V * M
			 */
			friend vec4 operator* (const vec4& v, const mat4& m) { return m * v; }
			/**
			 * Addition de matrice dans l'objet courant.
			 */
			mat4& operator+=(const mat4&);
			/**
			 * Soustraction de matrices dans l'objet courant.
			 */
			mat4& operator-=(const mat4&);
			/**
			 * Multiplication de la matrice courante par un scalaire.
			 */
			mat4& operator*=(const double);
			/**
			 * Division de la matrice courante par un scalaire.
			 */
			mat4& operator/=(const double);
			/**
			 * Multiplie la matrice par un vecteur. Retourne un nouveau vecteur.
			 */
			vec4 operator*=(const vec4&) const;
			/**
			 * @brief Reversed operator for vector-matrix multiplication
			 * @param v The vector to multiply
			 * @param m The matrix to use for the multiplication
			 * @return The operation of V * M
			 */
			friend vec4 operator*=(const vec4& v, const mat4& m) { return m *= v; }
			// }}}

			// Extraction et insertion pour entrée/sortie standard. {{{
			/**
			 * Opérateur d'extraction
			 */
			friend std::ostream& operator<<(std::ostream& os, const mat4& m) {
				return os << std::endl << m._m[0] << std::endl << m._m[1] << std::endl << m._m[2] << std::endl << m._m[3] << std::endl;
			}
			/**
			 * Opérateur d'insertion
			 */
			friend std::istream& operator>>(std::istream& is, mat4& m) {
				return is >> m._m[0] >> m._m[1] >> m._m[2];
			}
			// }}}
		protected:
			/**
			 * @brief Computes the determinant of the minor matrix extracted from the current matrix at the coordinates (r0r1r2,c0c1c2)
			 * @param r0 The first row of the minor matrix to take
			 * @param r1 The second row of the minor matrix to take
			 * @param r2 The third row of the minor matrix to take
			 * @param c0 The first column of the matrix to take
			 * @param c1 The second column of the matrix to take
			 * @param c2 The third column of the matrix to take
			 * @return The determinant of the minor matrix
			 */
			inline double minor(const int r0, const int r1, const int r2, const int c0, const int c1, const int c2) const;

			mat4 adjoint_matrix() const;

	};
	// }}}

	// Création de matrices de rotation sur X,Y,Z {{{
	/**
	 * Crée une matrice de rotation sur l'axe X
	 */
	inline mat4 rotateX(const double theta) {
		mat4 output;
		double alpha = DegToRad * theta;

		output[2][2] = output[1][1] = std::cos(alpha);
		output[2][1] = std::sin(alpha);
		output[1][2] = -output[2][1];

		return output;
	}
	/**
	 * Crée une matrice de rotation sur l'axe Y
	 */
	inline mat4 rotateY(const double theta) {
		mat4 output;
		double alpha = DegToRad * theta;

		output[2][2] = output[0][0] = std::cos(alpha);
		output[0][2] = std::sin(alpha);
		output[2][0] = -output[0][2];

		return output;
	}
	/**
	 * Crée une matrice de rotation sur l'axe Z
	 */
	inline mat4 rotateZ(const double theta) {
		mat4 output;
		double alpha = DegToRad * theta;

		output[1][1] = output[0][0] = std::cos(alpha);
		output[1][0] = std::sin(alpha);
		output[0][1] = -output[1][0];

		return output;
	}
	// }}}

	/**
	 * Crée une matrice de rotation selon un axe U à un angle de T degrés (en DEG) en utilisant la formule de Rodrigues
	 */
	mat3 freeRotate3D(const double, const vec3&);

	/**
	 * Crée une matrice de rotation comme indiqué dans le TP10 de HMIN212. Ne suit pas forcément la formule de Rodrigues
	 */
	mat3 customRotationMatrixFromAxis(const double, const vec3&);

	/**
	 * Permet de construire une matrice de translation (x,y,z)
	 */
	mat4 translate(const double, const double, const double);
	/**
	 * Permet de construire une matrice de translation représentée par le vecteur v
	 */
	mat4 translate(const vec3&);
	/**
	 * Permet de construire une matrice de translation représentée par le vecteur v
	 */
	mat4 translate(const vec4&);

	/**
	 * Crée une matrice de mise à l'échelle selon les paramètres donnés
	 */
	inline mat4 scale(const double, const double, const double);
	/**
	 * Crée une matrice de mise à l'échelle selon le vecteur donné
	 */
	inline mat4 scale(const vec3&);

	/**
	 * @brief Generate a frustum projection matrix using the provided arguments.
	 * @param bottom The distance to the bottom plane on the Y axis
	 * @param top The distance to the top plane on the Y axis
	 * @param left The distance to the left plane of the X axis
	 * @param right The distance to the right plane on the y axis
	 * @param near The distance to the near plane
	 * @param far The distance to the far plane
	 * @return A projection frustum used in shaders to specify how to project vertices on the projection plane.
	 */
	mat4 generate_projection_matrix(double bottom, double top, double left, double right, double near, double far);

	/**
	 * @brief Generates a look-at matrix, to compute camera position and orientation.
	 * @param from The point to look from
	 * @param to The point to look at
	 * @return A look-at matrix, with the `up` vector looking along +Y
	 */
	mat4 generate_lookat_matrix(vec4 from, vec4 to);

	/**
	 * @brief Generates a look-at matrix, to compute camera position and orientation.
	 * @param from The point to look from
	 * @param to The point to look at
	 * @param up The orientation of the camera (which way the top is)
	 * @return A look-at matrix
	 */
	mat4 generate_lookat_matrix(vec4 from, vec4 to, vec4 up);
}

#endif // _CORE_TRANSFORM_HPP_
