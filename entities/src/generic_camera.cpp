#include "../include/generic_camera.hpp"

#include <glm/common.hpp>
#include <glm/gtc/matrix_transform.hpp>

entities::generic_camera::generic_camera(core::vec4 position) {
	// Setup parameters for the camera : position, angle, and projection parameters
	this->transform = core::transform_t(position);
	this->rho = 0.01;
	this->theta = 270.01;
	this->radius = 1.;
	this->projection_parameters = proj_params();
	this->projection_parameters.left = -1.0;
	this->projection_parameters.right = 1.0;
	this->projection_parameters.bottom = -1.0;
	this->projection_parameters.top = 1.0;
	this->projection_parameters.near = .01;
	this->projection_parameters.far = 50.;
	this->projection_parameters.use_default_identity_matrix = false;
	this->projection_parameters.transpose_matrix = false;
	this->projection_parameters.print_info = false;
}

std::pair<core::vec4, core::vec4> entities::generic_camera::get_raycast_worldspace() const {
	return std::make_pair(core::vec4(), core::vec4());
}

void entities::generic_camera::update_projection_parameters(proj_params p) {
	this->projection_parameters.left = p.left;
	this->projection_parameters.right = p.right;
	this->projection_parameters.bottom = p.bottom;
	this->projection_parameters.top = p.top;
	this->projection_parameters.near = p.near;
	this->projection_parameters.far = p.far;
	this->projection_parameters.print_info = p.print_info;
	this->projection_parameters.transpose_matrix = p.transpose_matrix;
	this->projection_parameters.use_default_identity_matrix = p.use_default_identity_matrix;
}

void entities::generic_camera::update_projection_matrix() {
	if (this->projection_parameters.use_default_identity_matrix) {
		this->projection = core::mat4(1.0);
	} else {
		this->projection = core::mat4(glm::ortho(
			this->projection_parameters.left,	this->projection_parameters.right,
			this->projection_parameters.bottom,	this->projection_parameters.top,
			this->projection_parameters.near,	this->projection_parameters.far
		));
		if (this->projection_parameters.transpose_matrix == false) {
			this->projection.transpose_self();
		}
	}
	if (this->projection_parameters.print_info == true) {
		std::cout << this->projection_parameters << std::endl;
	}
}

void entities::generic_camera::update_view_matrix() {
	core::vec4 to = core::vec4(this->transform.get_translation()) + this->compute_heading_vector();
	//core::vec4 newpos = core::vec4(this->transform.get_translation()) + -(this->compute_heading_vector());
	this->view_matrix = core::mat4(glm::lookAt(glm::vec3(
			this->transform.get_translation().x,
			this->transform.get_translation().y,
			this->transform.get_translation().z
	), glm::vec3(to.x, to.y, to.z), glm::vec3(.0,1.,.0)));
}

void entities::generic_camera::rotate_rho(double angle) {
	rho += angle;
	if ( rho > ( 90.) ) {rho = 89.9;}
	if ( rho < (-90.) ) {rho =-89.9;}
}

void entities::generic_camera::rotate_theta(double angle) {
	theta += angle;
	if (theta > 360.) { theta -= 360.; }
	if (theta < .0 ) { theta += 360.; }
}

void entities::generic_camera::move_x(double dist) {
	this->transform.translate_x(dist);
}

void entities::generic_camera::move_y(double dist) {
	this->transform.translate_y(dist);
}

void entities::generic_camera::move_z(double dist) {
	this->transform.translate_z(dist);
}

void entities::generic_camera::move_backwards(double scalar) {
	core::vec4 pos = this->transform.get_translation_vec4();
	core::vec4 to = this->compute_heading_vector();
	this->transform.set_translation(pos - to * scalar);
}

void entities::generic_camera::move_forward(double scalar) {
	core::vec4 pos = this->transform.get_translation_vec4();
	core::vec4 to = this->compute_heading_vector();
	this->transform.set_translation(pos + to * scalar);
}

void entities::generic_camera::move_left(double scalar) {
	core::vec4 pos = this->transform.get_translation_vec4();
	core::vec4 to = this->compute_heading_vector();
	core::vec4 up = core::vec4(.0,-1.,.0,.0);
	core::vec4 right = up.cross(to);
	this->transform.set_translation(pos - right * scalar);
}

void entities::generic_camera::move_right(double scalar) {
	core::vec4 pos = this->transform.get_translation_vec4();
	core::vec4 to = this->compute_heading_vector();
	core::vec4 up = core::vec4(.0,-1.,.0,.0);
	core::vec4 right = up.cross(to);
	this->transform.set_translation(pos + right * scalar);
}

void entities::generic_camera::move_down(double scalar) {
	core::vec4 pos = this->transform.get_translation_vec4();
	core::vec4 to = this->compute_heading_vector();
	core::vec4 fake_up = core::vec4(.0,-1.,.0,.0);
	core::vec4 right = to.cross(fake_up);
	core::vec4 true_up = right.cross(to);
	this->transform.set_translation(pos - true_up * scalar);
}

void entities::generic_camera::move_up(double scalar) {
	core::vec4 pos = this->transform.get_translation_vec4();
	core::vec4 to = this->compute_heading_vector();
	core::vec4 fake_up = core::vec4(.0,-1.,.0,.0);
	core::vec4 right = to.cross(fake_up);
	core::vec4 true_up = right.cross(to);
	this->transform.set_translation(pos + true_up * scalar);
}

core::vec4 entities::generic_camera::compute_heading_vector() const {
	double theta_rad = this->theta * core::DegToRad;
	double rho_rad = this->rho * core::DegToRad;

	// Compute angles and take irregularities into account
	double cos_rho = std::cos(rho_rad); if (NEAR_ZERO(cos_rho)) { cos_rho = (rho > .0) ? 1. : -1.; }
	double sin_rho = std::sin(rho_rad); if (NEAR_ZERO(sin_rho)) { sin_rho = 1. ; }
	double cos_theta = std::cos(theta_rad); if (NEAR_ZERO(cos_theta)) { cos_theta = (theta < 180.) ? 1. : -1.; }
	double sin_theta = std::sin(theta_rad); if (NEAR_ZERO(sin_theta)) { sin_theta = (theta >  90. && theta < 270.) ? 1. : -1.; }

	double x = (1./this->radius) * sin_theta * cos_rho;
	double y = (1./this->radius) * sin_rho;
	double z = (1./this->radius) * cos_theta * cos_rho;
	return core::vec4(x,y,z,.0);
}

void entities::generic_camera::print_info() const {
	std::cerr << "Position at : " << this->transform.get_translation() << std::endl;
	std::cerr << "Rho at : " << this->rho << std::endl;
	std::cerr << "Theta at : " << this->theta << std::endl;
}
