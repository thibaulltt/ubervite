/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "../include/glwidget.h"
#include <QMouseEvent>
#include <QCoreApplication>
#include <QLayout>
#include <math.h>
#include <iostream>
#include <fstream>
#include <sstream>
#ifndef __gl_h_
#include <GL/glew.h>
#endif

#include "../../loaders/include/obj_loader.hpp"
#include "../../helpers/include/files.hpp"
#include "../include/debug_panel.hpp"

#include "../../entities/include/fps_camera.hpp"

GLWidget::GLWidget(size_t refresh_rate, QWidget *parent) : QOpenGLWidget(parent), func_4_1_core(nullptr) {
	this->setFocus();
	this->grabKeyboard();
	this->is_locked_to_gl_widget = true;

	// Setup attributes for the creation of the OpenGL context.
	this->setAttribute(Qt::WA_DeleteOnClose, true);
	this->format().setVersion(4, 1);
	this->format().setSamples(16);

	camera = new entities::fps_camera(core::vec4(7.,4.,-6.,1.));
	camera->update_view_matrix();
	camera->update_projection_matrix();

	// Connect refresh timer to refresh the screen
	this->refreshTimer = new QTimer(this);
	this->refreshTimer->setSingleShot(false);		// make the timer repeat the interval provided
	this->refreshTimer->setTimerType(Qt::PreciseTimer);	// keep precise time measurements
	connect(this->refreshTimer, &QTimer::timeout, this, &GLWidget::custom_update);
	// Every 6.94 milliseconds for 144Hz, 16.66ms for 60Hz, and 33.33ms for 30Hz
	this->refreshTimer->start(static_cast<std::chrono::milliseconds>(1000/refresh_rate));
}

void GLWidget::initializeGL() {
	/**
	 * Here, the OpenGL context is initialized. As such, we can initialize the needed functions
	 * and get the functions for the current version.
	 */
	this->initializeOpenGLFunctions();
	// Get OpenGL 4.1 function pointers
	this->func_4_1_core = this->context()->versionFunctions<QOpenGLFunctions_4_1_Core>();
	this->func_4_1_compat = this->context()->versionFunctions<QOpenGLFunctions_4_1_Compatibility>();

	// Load scene
	this->sm = parseObjFile(this->context(), "../models/ubervite_scene_flipped_5.dae");

	if (this->sm->get_drawable_elements().size() == 0) {
		std::cerr << "glwidget.cpp : 98 : Could not parse object file. Exiting program ..." << std::endl;
		this->cleanup();
		emit this->destroyed();
		exit(EXIT_FAILURE);
	}

	// Initialise draw arrays and context
	this->sm->request_object_initialization();

	this->func_4_1_core->glEnable(GL_DEPTH_TEST);
	this->func_4_1_core->glDepthFunc(GL_LEQUAL);
	this->func_4_1_core->glEnable(GL_CULL_FACE);
	this->func_4_1_core->glCullFace(GL_BACK);
}

void GLWidget::paintGL() {
	// Clear the screen
	this->func_4_1_core->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	this->func_4_1_core->glClearColor(.2f, .2f, .6f, 0.0f);

	camera->update_view_matrix();

	this->sm->render();
}

void GLWidget::resizeGL(int w, int h) {
	this->func_4_1_compat->glViewport(0,0,w,h);
	double dw = static_cast<double>(w);
	double dh = static_cast<double>(h);
	dw /= dh;
	dh /= dh;
	this->update_projection_matrix(dh/2.,dw/2.);
	emit this->camera_inspector_update_request();
}

void GLWidget::update_projection_matrix(double dh, double dw) {
	camera->projection_parameters.left = -dw;
	camera->projection_parameters.right = dw;
	camera->projection_parameters.bottom = -dh;
	camera->projection_parameters.top = dh;
	camera->update_projection_matrix();
}

void GLWidget::camera_inspector_message() {
	this->custom_update();
}

void GLWidget::toggle_raycast_collision() {
	drawable* di = this->sm->get_raycast_intersection(camera->get_raycast_worldspace());
	if (di != nullptr) {
		di->toggle_hidden();
	} else {
		std::cerr << "Could not find any intersections" << std::endl;
	}
}

void GLWidget::mousePressEvent(QMouseEvent *event) {
	m_lastPos = event->pos();
	if (this->is_locked_to_gl_widget) {
		// Do something here ...
	} else {
		std::cerr << "Grabbing kbm" << std::endl;
		this->grabKeyboard();
		this->setFocus();
		this->cursor().setShape(Qt::CursorShape::BlankCursor);
		this->cursor().setPos(m_lastPos);
		this->is_locked_to_gl_widget = true;
	}
}

void GLWidget::mouseMoveEvent(QMouseEvent *event) {
	// TODO : get accurate mouse movement from Qt
}

void GLWidget::keyPressEvent(QKeyEvent *event) {
	int key_pressed = event->key();
	switch (key_pressed) {
		case Qt::Key_Z:
			camera->move_forward(.5);
			break;
		case Qt::Key_Q:
			camera->move_left(.5);
			break;
		case Qt::Key_S:
			camera->move_backwards(.5);
			break;
		case Qt::Key_D:
			camera->move_right(.5);
			break;
		case Qt::Key_A:
			camera->move_down(.5);
			break;
		case Qt::Key_E:
			camera->move_up(.5);
			break;
		case Qt::Key_R:
			this->toggle_raycast_collision();
			break;
		case Qt::Key_Up:
			camera->rotate_rho(-.75);
			break;
		case Qt::Key_Down:
			camera->rotate_rho(.75);
			break;
		case Qt::Key_Left:
			camera->rotate_theta(-1.5);
			break;
		case Qt::Key_Right:
			camera->rotate_theta(1.5);
			break;
		case Qt::Key_PageUp:
			camera->zoom_in();
			break;
		case Qt::Key_PageDown:
			camera->zoom_out();
			break;
		case Qt::Key_Escape:
			if (this->is_locked_to_gl_widget) {
				std::cerr << "Releasing keyboard and mouse" << std::endl;
				this->releaseKeyboard();
				this->cursor().setShape(Qt::CursorShape::ArrowCursor);
				this->is_locked_to_gl_widget = false;
			} else {
				this->cleanup();
				this->close();
			}
			break;
		default:
			event->accept();
			break;
	}
	camera->update_view_matrix();
	emit this->camera_inspector_update_request();
}

void GLWidget::keyReleaseEvent(QKeyEvent *event) {
	// nothing here ...
}

void GLWidget::closeEvent(QCloseEvent *event) {
	emit QWidget::destroyed(this);
	event->accept();
}

void GLWidget::custom_update() {
	this->update();
}

QSize GLWidget::minimumSizeHint() const {
	return QSize(50, 50);
}

QSize GLWidget::sizeHint() const {
	return QSize(1366, 768);
}

void GLWidget::cleanup() {
	makeCurrent();
	doneCurrent();
}
