/**==========================================
 * FILE : helpers/include/xml_attributes.hpp
 * AUTH : Thibault de VILLÈLE
 * DATE : 31/10/2019
 * DESC : Contient la définition d'un dictionnaire
 * 	  de valeurs pour une balise XML. Est
 * 	  utilisé pour la lecture d'un fichier XML.
 * ==========================================
 */

#ifndef _HELPERS_INCLUDE_XML_ATTRIBUTES_HPP_
#define _HELPERS_INCLUDE_XML_ATTRIBUTES_HPP_

#include <vector>
#include <string>

#include "xml_types.hpp"

#define PARSING_DEBUG

namespace helpers {

	namespace xml {

		class attributes {
			protected:
				/* Properties list */
				std::vector<std::string> property;
				/* Associated values */
				std::vector<std::string> value;
			public:
				// default constructor
				attributes();
				// constructs the dictionnary from a given string
				attributes(const token&);
				// add attribute pair to the the attributes of the tag
				void add_attribute(const std::string&, const std::string&);
				// get a value for a property
				std::string operator()(const std::string&) const;
		};

	}

}

#endif // _HELPERS_INCLUDE_XML_ATTRIBUTES_HPP_
