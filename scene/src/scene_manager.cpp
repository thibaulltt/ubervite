#include "../include/scene_manager.hpp"

#include "../../entities/include/generic_camera.hpp"

managers::scene_manager::scene_manager() {
	this->root_objects.clear();
	this->lights.clear();
	this->drawable_elements.clear();
	this->interactible_elements.clear();
	this->camera = ::camera;
}

void managers::scene_manager::set_lights(std::vector<core::mat4> new_lights) {
	this->lights = new_lights;
}

void managers::scene_manager::set_drawable(std::vector<drawable*> new_elements) {
	this->drawable_elements = new_elements;
}

const std::vector<core::mat4>& managers::scene_manager::get_lights() const { return this->lights; }

const std::vector<drawable*>& managers::scene_manager::get_drawable_elements() const { return this->drawable_elements; }

void managers::scene_manager::add_scene_object(scene::scene_object *to_add) {
	this->root_objects.push_back(to_add);
}

void managers::scene_manager::add_scene_objects(std::vector<scene::scene_object *> to_add) {
	for (const auto& so : to_add) {
		this->root_objects.push_back(so);
	}
}

void managers::scene_manager::set_root_objects(std::vector<scene::scene_object *> to_add) {
	this->root_objects = to_add;
}

const entities::generic_camera* managers::scene_manager::get_camera() const {
	return this->camera;
}

scene::scene_object* managers::scene_manager::search_by_string(const std::string query) const {
	scene::scene_object* search_res = nullptr;
	for (uint i = 0; i < this->root_objects.size(); ++i) {
		search_res = this->root_objects[i]->recursive_name_search(query);
		if (search_res != nullptr) {
			return search_res;
		}
	}
	return nullptr;
}

std::vector<scene::scene_object*> managers::scene_manager::search_all_by_string(const std::string query) const {
	std::vector<scene::scene_object*> search_res;
	for (uint i = 0; i < this->root_objects.size(); ++i) {
		std::vector<scene::scene_object*> objs;
		this->root_objects[i]->recursive_full_name_search(query, objs);
		for (const auto& r : objs) {
			search_res.push_back(r);
		}
	}
	return search_res;
}

drawable* managers::scene_manager::get_raycast_intersection(const std::pair<core::vec4, core::vec4>& ray) const {
	double min_dist = std::numeric_limits<double>::max();
	drawable* closest_obj = nullptr;
	std::cerr << "Iterating over the " << this->root_objects.size() << " root objects" << std::endl;
	for (const auto& c : this->root_objects) {
		std::pair<const drawable*, double> p = c->check_raycast(ray);
		if (p.second < min_dist) {
			min_dist = p.second;
			closest_obj = const_cast<drawable*>(p.first);
		}
	}
	return closest_obj; // Can be nullptr !
}

void managers::scene_manager::request_object_initialization() const {
	for (const auto& m : this->drawable_elements) {
		m->init_gl();
	}
}

void managers::scene_manager::render() const {
	for (const auto& o : this->root_objects) {
		o->render(core::mat4(1.0));
	}
}
