#include "../include/xml_attributes.hpp"

#include <iostream>

using namespace helpers::xml;

attributes::attributes() {
	this->property.resize(0);
	this->value.resize(0);
}

/*
attributes::attributes(const token& t) {
	enum subtoken_t {
		STRAY,
		PROPERTY,
		VALUE
	};

	bool entered_scope = false, between_quotes = false, last_subtoken_was_property = false;
	std::vector<std::string> valid_subtokens = std::vector<std::string>();
	std::string subtoken = "";
	subtoken_t last_subtoken_type = subtoken_t::STRAY;
	// Iterate on the string given, with those rules (no particular order) :
	// 0	- the operating space of this funcion is between the two delimiting characters : < and >
	// 1	- whitespace (space, tab...) end a property's name, without question
	// 2	- there must be an equal sign after a property name, with optional
	// 		whitespace (if not, discard property)
	// 3	- double quotes are the __only__ way to define a value
	// 4	- backslashes in values means we disregard the next character,
	// 		unless the backslash is the last character in the string,
	// 		in which case we remove it (sanitizing input)
	// 5	- a value must end at the first un-escaped double quote sign
	// 6	- a property can only have one value (discarded if not)
	// 7	- a value cannot be left alone (discarded if left alone)
	// 8	- whitespace between the end of a value and the beginning of a
	// 		property is optional
	// 9	- if a forward slash is found, check if the next char is a
	// 		'greater than' sign ('>'). if not, then discard the forward
	// 		slash and continue parsing
	for (size_t i = 0; i < t.content.size(); ++i) {
		// get current char
		char c = t.content.at(i);
		// apply rule 0
		if (c == '<' && !entered_scope) {
			entered_scope = true;
			continue;
		}
		if (entered_scope) {
			switch (c) {
				case '\\':
					// first case, we need to ignore next character
					#ifdef PARSING_DEBUG
					if (i == t.content.size()-2) { std::cerr << "Stray backspace at the end of the token. Ignoring it." << std::endl; }
					#endif
					i++;
					continue;
				case '<':
					#ifdef PARSING_DEBUG
					std::cerr << "Parser : disregarded \"<\" character at index" << i << "." << std::endl;
					#endif
					continue;
				case '"':
					if (between_quotes == false) {
						between_quotes = true;
						continue;
					} else {
						// we left quotes :
						if (last_subtoken_type == subtoken_t::PROPERTY) {
							this->value.push_back(subtoken);
							subtoken = "";
							last_subtoken_type = subtoken_t::VALUE;
						}
					}
					break;
				case ' ':
					[[fallthrough]];
				case '\t':
					[[fallthrough]];
				case '\r':
					[[fallthrough]];
				case '\n':
					[[fallthrough]];
				case '\f':
					[[fallthrough]];
				case '\v':
					if (!between_quotes) {
						// here, a subtoken has reached full capacity.
						// if we were parsing a value before, and we're ont between quotes :
						switch (last_subtoken_type) {
							case subtoken_t::PROPERTY:
								break;
							case subtoken_t::VALUE:
								break;
							default:
								break;
						}
					}
					continue;
			}
		}
	}
}
*/
