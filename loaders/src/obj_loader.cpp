#include "../include/obj_loader.hpp"

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

managers::scene_manager* parseObjFile(QOpenGLContext* context, const char* path_name) {
	const aiScene* scene = aiImportFile(path_name, 0x0);
	if (!scene) {
		std::cerr << "obj_loader.cpp : 10 : OBJ File Loading failed, here's the error message :" << std::endl;
		std::cerr << aiGetErrorString() << std::endl;
		return nullptr;
	}
	// Create a scene manager
	managers::scene_manager* sm = new managers::scene_manager();

	if (scene->HasCameras()) {
		// core::transform_t camera_pos = get_camera_info(scene->mCameras[0]);
		// TODO : set camera pos, angle, and heading from there
	}
	if (scene->HasLights()) {
		std::vector<core::mat4> lights;
		for (uint li = 0; li < scene->mNumLights; ++li) {
			lights.push_back(get_light_info(scene->mLights[li]));
		}
		// TODO : add lights from here to the scene manager
		// sm->set_lights(lights);
	}
	if(scene->HasMeshes()) {
		std::vector<drawable*> meshes;
		for (uint mi = 0; mi < scene->mNumMeshes; ++mi) {
			meshes.push_back(new mesh(context, scene, scene->mMeshes[mi]));
		}
		sm->set_drawable(meshes);
	}

	// Get the root objects, once everything else has been loaded.
	std::vector<scene::scene_object*> root_objs;
	scene::scene_object* rootobj = new scene::scene_object(sm, scene, scene->mRootNode);
	if (rootobj != nullptr) {
		root_objs.push_back(rootobj);
	} else {
		std::cerr << "No nodes were parsed at the root of the loaded aiScene. Exiting ..." << std::endl;
		return nullptr;
	}
	sm->set_root_objects(root_objs);

	return sm;
}

core::mat4 get_light_info(const aiLight* light) {
	aiVector3D pos = light->mPosition;
	aiColor3D  amb = light->mColorAmbient;
	aiColor3D  dif = light->mColorDiffuse;
	aiColor3D  spe = light->mColorSpecular;
	return core::mat4(
		static_cast<double>(amb.r), static_cast<double>(amb.g), static_cast<double>(amb.b), static_cast<double>(pos.x),
		static_cast<double>(dif.r), static_cast<double>(dif.g), static_cast<double>(dif.b), static_cast<double>(pos.y),
		static_cast<double>(spe.r), static_cast<double>(spe.g), static_cast<double>(spe.b), static_cast<double>(pos.z),
		.0, .0, .0, 1.
	);
}

