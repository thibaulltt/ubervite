#ifndef _QT_INCLUDE_DEBUG_PANEL_HPP_
#define _QT_INCLUDE_DEBUG_PANEL_HPP_

#include <QWidget>
#include <QTabWidget>

#include "./inspectors.hpp"

QT_FORWARD_DECLARE_CLASS(GLWidget)

/**
 * @brief Right-attached debug panel for (currently) the camera and the projection parameters of the engine.
 */
class debug_panel : public QTabWidget {
		Q_OBJECT
	protected:
		camera_inspector* camera_insp;
		GLWidget* main_screen;
	public:
		debug_panel(GLWidget* gl_widget);
	protected slots:
		void closeEvent(QCloseEvent *event) override;
};

#endif // _QT_INCLUDE_DEBUG_PANEL_HPP_
