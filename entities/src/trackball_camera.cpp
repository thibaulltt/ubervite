#include "../include/trackball_camera.hpp"

void entities::trackball_camera::update_view_matrix() {
	core::vec4 to = core::vec4(this->transform.get_translation()) + this->compute_heading_vector();
	//core::vec4 newpos = core::vec4(this->transform.get_translation()) + -(this->compute_heading_vector());
	this->view_matrix = core::mat4(glm::lookAt(glm::vec3(
			this->transform.get_translation().x,
			this->transform.get_translation().y,
			this->transform.get_translation().z
	), glm::vec3(to.x, to.y, to.z), glm::vec3(.0,1.,.0)));
}
