#ifndef _OPENGL_INCLUDE_GENERIC_DRAWABLE_OBJECT_HPP_
#define _OPENGL_INCLUDE_GENERIC_DRAWABLE_OBJECT_HPP_

#include <QOpenGLBuffer>
#include <QOpenGLContext>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>

#include "../../core/core_include_all.hpp"

// TODO : Add a QOpenGLTexture, or maybe a few :shrug:
// TODO : add an inspector (friend the class and make it appear on a window)

/**
 * @brief The drawable class implements a basic functionnality abstraction layer to be able to draw an object on screen.
 */

class drawable : public QOpenGLFunctions {
	protected:
		/**
		 * @brief Shader program associated with the rendered model
		 */
		QOpenGLShaderProgram* program;
		/**
		 * @brief Vertex Array Object to hold the models' data
		 */
		QOpenGLVertexArrayObject vao;
		/**
		 * @brief Buffer for the vertex data of the model
		 */
		QOpenGLBuffer vertex_buffer;
		/**
		 * @brief Buffer for the normals of the model
		 */
		QOpenGLBuffer normal_buffer;
		/**
		 * @brief Buffer for the colors of the model's vertices
		 */
		QOpenGLBuffer color_buffer;
		/**
		 * @brief Buffer for the UV coordinates of the model
		 */
		QOpenGLBuffer uv_buffer;
		/**
		 * @brief Buffer for the faces of the object
		 */
		QOpenGLBuffer face_buffer;
		/**
		 * @brief Drawing mode for the drawable object. Can be GL_TRIANGLES, GL_LINES, ...
		 */
		GLenum draw_mode;
		/**
		 * @brief Variable specifying the number of elements to draw
		 */
		GLsizei elements_to_draw;
		/**
		 * @brief Path to the currently loaded vertex shader.
		 */
		std::string vertex_shader_path;
		/**
		 * @brief Path to the currently loaded fragment shader.
		 */
		std::string fragment_shader_path;
		/**
		 * @brief Defines if the object can be rendered or not.
		 */
		bool is_hidden;
		/**
		 * @brief Projection matrix location in QOpenGLShaderProgram
		 */
		int projection_matrix_location;
		/**
		 * @brief view_matrix_location in QOpenGLShaderProgram
		 */
		int view_matrix_location;
		/**
		 * @brief model_matrix_location in QOpenGLShaderProgram
		 */
		int model_matrix_location;
		/**
		 * @brief light_position_worldspace_location in QOpenGLShaderProgram
		 */
		int light_position_worldspace_location;
	public:
		/**
		 * @brief Default constructor for a drawable object.
		 * @param context OpenGL context to draw in.
		 * @param vpath Path to the vertex shader.
		 * @param fpath Path to the fragment shader.
		 * @param dm Draw mode to use for this drawable object.
		 */
		drawable(QOpenGLContext* context,
			 const std::string vpath = "../shaders/base_vshader.vert",
			 const std::string fpath = "../shaders/base_fshader.frag",
			 GLenum dm = GL_TRIANGLES);
		/**
		 * @brief Default destructor for the drawable. Frees up all allocated OpenGL memory, and exits.
		 */
		virtual ~drawable();
		/**
		 * @brief Initialise OpenGL objects
		 */
		virtual void init_gl();
		/**
		 * @brief Setup VAO/VBO attributes and data
		 */
		virtual void setup_vertex_attribs();
		/**
		 * @brief Updates vertex data within the VBO objects
		 */
		virtual void update_vertex_data();
		/**
		 * @brief Sets the vertex shader path to the new vpath value.
		 * @param vpath New path to the vertex shader
		 */
		void set_vertex_shader_path(const std::string vpath);
		/**
		 * @brief Sets the fragment shader path to the new fpath value.
		 * @param fpath New path to the fragment shader
		 */
		void set_fragment_shader_path(const std::string fpath);
		/**
		 * @brief Sets the visibility of the drawable object.
		 * @param visibility True if the object needs to be visible, false if not
		 */
		void set_hidden(bool visibility);
		/**
		 * @brief Toggles the visibility of the object.
		 */
		void toggle_hidden();
		/**
		 * @brief Sets a new draw mode for this drawable.
		 * @param dm New draw mode for this drawable
		 */
		void set_draw_mode(GLenum dm);
		/**
		 * @brief Renders the current object with the view matrix provided by the camera
		 * @param model_matrix The model matrix of the current object.
		 */
		virtual void render_object(const core::mat4 model_matrix);
		/**
		 * @brief Check ray intersection against the current object.
		 * @param ray The ray to check the intersection with.
		 * @return The point where the ray intersected if it did, the point where the ray began otherwise.
		 * @note In the generic drawable class, this function always returns no intersection.
		 * That it because in this class, there is nothing to check the ray against.
		 */
		virtual std::pair<core::vec4, double> check_raycast_intersection(const std::pair<core::vec4, core::vec4>& ray) const;
	protected:
		/**
		 * @brief Load shaders for the current object.
		 * @param vpath Path to the vertex shader
		 * @param fpath Path to the fragment shader
		 */
		void load_shaders();

};

#endif // _OPENGL_INCLUDE_GENERIC_DRAWABLE_OBJECT_HPP_
