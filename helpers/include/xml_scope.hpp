/**==========================================
 * FILE : helpers/include/xml_attributes.hpp
 * AUTH : Thibault de VILLÈLE
 * DATE : 31/10/2019
 * DESC : Contient la définition d'un dictionnaire
 * 	  de valeurs pour une balise XML. Est
 * 	  utilisé pour la lecture d'un fichier XML.
 * ==========================================
 */

#ifndef _HELPERS_INCLUDE_XML_SCOPE_HPP_
#define _HELPERS_INCLUDE_XML_SCOPE_HPP_

#include <vector>
#include <string>

#include "xml_types.hpp"
#include "xml_attributes.hpp"

namespace helpers {

	namespace xml {

		/* Represents the scope of a XML tag. And it's associated content(s) */
		class scope {
			public:
				/* Name of the scope element */
				std::string name;
				/* Attributes of it */
				attributes properties;
				/* Optional raw content of the scope (text, perhaps ?) */
				std::vector<std::string> content;
				/* Child scopes */
				std::vector<scope*> children;
				/* Parent scope */
				scope* parent;
			public:
				scope(scope*);
				scope(const std::string&, const attributes&);
		};

	}

}

#endif // _HELPERS_INCLUDE_XML_SCOPE_HPP_
