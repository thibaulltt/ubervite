#include "../include/transform.hpp"

using namespace core;

transform_t::transform_t() : translation(core::vec3()), rotation(core::vec3()), scaling(core::vec3(1.,1.,1.)) {}

transform_t::transform_t(const vec4& v) {
	// Initializes the object at the position specified by the vector :
	this->translation = core::vec3(v.x, v.y, v.z);
	this->rotation = core::vec3();
	this->scaling = core::vec3(1.,1.,1.);
}

transform_t::transform_t(const core::mat4& transform) {
	core::mat4 t = transform;
	// Get the translation part, that one's easy :
	this->translation = core::vec3(t[0][3], t[1][3], t[2][3]);
	// Zero out the translation in the matrix :
	t[0][3] = .0; t[1][3] = .0; t[2][3] = .0;

	double sx = core::vec3(t[0][0], t[1][0], t[2][0]).length();
	double sy = core::vec3(t[0][1], t[1][1], t[2][1]).length();
	double sz = core::vec3(t[0][2], t[1][2], t[2][2]).length();
	this->scaling = core::vec3(sx, sy, sz);

//	t[0][0] /= sx; t[0][1] /= sy; t[0][2] /= sz;
//	t[1][0] /= sx; t[1][1] /= sy; t[1][2] /= sz;
//	t[2][0] /= sx; t[2][1] /= sy; t[2][2] /= sz;

	this->rotation = core::vec3();
}

transform_t::transform_t(const transform_t& t) : translation(t.translation), rotation(t.rotation), scaling(t.scaling) {}

transform_t::transform_t(transform_t&& t) : translation(t.translation), rotation(t.rotation), scaling(t.scaling) {}

transform_t& transform_t::operator= (const transform_t &t) {
	this->translation = t.translation;
	this->rotation = t.rotation;
	this->scaling = t.scaling;
	return *this;
}

transform_t& transform_t::scale(const double &x, const double &y, const double &z) {
	this->scaling.x *= x;
	this->scaling.y *= y;
	this->scaling.z *= z;
	return *this;
}

transform_t& transform_t::scale(const core::vec3& v) {
	this->scaling.x *= v.x;
	this->scaling.y *= v.y;
	this->scaling.z *= v.z;
	return *this;
}

transform_t& transform_t::scale_x(const double &x) {
	this->scaling.x *= x;
	return *this;
}

transform_t& transform_t::scale_y(const double &y) {
	this->scaling.y *= y;
	return *this;
}

transform_t& transform_t::scale_z(const double &z) {
	this->scaling.z *= z;
	return *this;
}

transform_t& transform_t::rotate_x(const double &angle) {
	this->rotation.x += angle;
	return *this;
}

transform_t& transform_t::rotate_y(const double &angle) {
	this->rotation.y += angle;
	return *this;
}

transform_t& transform_t::rotate_z(const double &angle) {
	this->rotation.z += angle;
	return *this;
}

transform_t& transform_t::translate_x(const double &x) {
	this->translation.x += x;

	return *this;
}

transform_t& transform_t::translate_y(const double &y) {
	this->translation.y += y;

	return *this;
}

transform_t& transform_t::translate_z(const double &z) {
	this->translation.z += z;

	return *this;
}

transform_t& transform_t::translate(const double& x, const double& y, const double& z) {
	this->translation.x += x;
	this->translation.y += y;
	this->translation.z += z;

	return *this;
}

transform_t& transform_t::translate(const core::vec3& v) {
	this->translation.x += v.x;
	this->translation.y += v.y;
	this->translation.z += v.z;

	return *this;
}

core::mat4 transform_t::get_position() const {
	core::mat4 t = core::mat4();
	t[0][3] = this->translation.x;
	t[1][3] = this->translation.y;
	t[2][3] = this->translation.z;

	core::mat4 r = core::mat4(core::rotateZ(this->rotation.z) *
				  core::rotateY(this->rotation.y) *
				  core::rotateX(this->rotation.x));

	core::mat4 s = core::mat4();
	s[0][0] = this->scaling.x;
	s[1][1] = this->scaling.y;
	s[2][2] = this->scaling.z;

	return t * r * s;
}

core::mat4 transform_t::get_position_opengl() const {
	return this->get_position().transpose_self();
}

core::mat4 transform_t::get_inverse() const {
	return this->get_position().inverse();
}

core::mat4 transform_t::get_inverse_opengl() const {
	return this->get_inverse().transpose_self();
}

core::mat4 transform_t::get_rotation_matrix() const {
	return core::rotateX(this->rotation.x) * core::rotateY(this->rotation.y) * core::rotateZ(this->rotation.z);
}

const core::vec3& transform_t::get_translation() const { return this->translation; }

const core::vec3& transform_t::get_rotation() const { return this->rotation; }

const core::vec3& transform_t::get_scaling() const { return this->scaling; }

core::vec4 transform_t::get_translation_vec4() const { return this->translation; }

core::vec4 transform_t::get_rotation_vec4() const { return core::vec4(this->rotation); }

core::vec4 transform_t::get_scaling_vec4() const { return core::vec4(this->scaling); }

void transform_t::set_translation(core::vec4 v) { this->translation = core::vec3(v.x, v.y, v.z); }

void transform_t::set_rotation(core::vec4 v) { this->rotation = core::vec3(v.x, v.y, v.z); }

void transform_t::set_scaling(core::vec4 v) { this->scaling = core::vec3(v.x, v.y, v.z); }
