#ifndef _OPENGL_INCLUDE_DRAWABLE_MESH_HPP_
#define _OPENGL_INCLUDE_DRAWABLE_MESH_HPP_

#include "../../core/core_include_all.hpp"
#include "./generic_drawable_object.hpp"
#include "./drawable_bounding_box.hpp"

/**
 * @brief The mesh class represents a drawable mesh.
 * @details It is complete, with vertices, normals, color data, uvs, and face buffers. This class allows
 * for a mesh to be rendered. Although one caveat is that the mesh MUST be static. It can move, but it
 * cannot dynamically change the number of its vertices.
 */
class mesh : public drawable {
	protected:
		/**
		 * @brief Vertices of the model
		 */
		std::vector<core::vec4> vertices;
		/**
		 * @brief Normals of the model
		 */
		std::vector<core::vec4> normals;
		/**
		 * @brief Colors of the vertices for the model
		 */
		std::vector<core::vec4> colors;
		/**
		 * @brief UV coordinates of the model
		 */
		std::vector<core::vec2> uvs;
		/**
		 * @brief Faces indices of the model
		 */
		std::vector<uint32_t> faces;
		/**
		 * @brief Object-oriented-bounding-box for the current model
		 */
		core::bounding_box oobb;
		/**
		 * @brief A copy of the bounding box, drawable
		 */
		drawable_bounding_box* bb;
	public:
		/**
		 * @brief Contructor for a generic mesh from a scene imported by ASSIMP
		 * @param context The opengl context the mesh will be drawn to
		 * @param scene The aiScene from ASSIMP
		 * @param imported_mesh The aiMesh from ASSIMP
		 */
		mesh(QOpenGLContext* context, const aiScene* scene, const aiMesh* imported_mesh);
		/**
		 * @brief Checks for a raycast intersection.
		 * @param ray The ray to check against
		 * @return The point where the ray intersected if it did, the point where the ray began otherwise.
		 */
		virtual std::pair<core::vec4, double> check_raycast_intersection(const std::pair<core::vec4, core::vec4>& ray) const override;
		/**
		 * @brief Initializes the OpenGL VAO/VBO buffers, and sets up the necessary elements to draw the mesh.
		 */
		virtual void init_gl() override;
		/**
		 * @brief Sets up the VAO object.
		 */
		virtual void setup_vertex_attribs() override;
		/**
		 * @brief A custom render routine for the object. Used only to render either the object or it's bounding box, for debug.
		 * @param model_matrix The current model matrix
		 */
		virtual void render_object(const core::mat4 model_matrix) override;
		/**
		 * @brief Destroys the mesh
		 */
		virtual ~mesh() override;
};

#endif // _OPENGL_INCLUDE_DRAWABLE_MESH_HPP_
