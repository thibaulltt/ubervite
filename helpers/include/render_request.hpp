#ifndef RENDER_REQUEST_HPP
#define RENDER_REQUEST_HPP

#include "../../core/include/vector.hpp"
#include "../../core/include/matrix.hpp"

namespace helpers {

	// ABANDONNED
	class render_request {
		public:
			std::vector<core::vec3>& vertices;
			std::vector<core::vec3>& normals;
			std::vector<core::vec2>& uvs;
			std::vector<size_t>& elements;
	};
}

#endif // RENDER_REQUEST_HPP
