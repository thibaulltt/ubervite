#version 410 core
#line 1
#extension GL_ARB_gpu_shader_fp64 : enable

layout (location = 0) in vec4 vertex_position;
layout (location = 1) in vec4 vertex_normal;
layout (location = 2) in vec4 vertex_color;
layout (location = 3) in vec4 vertex_uv;

uniform mat4 projection_matrix;
uniform mat4 view_matrix;
uniform mat4 model_matrix;
uniform vec4 light_position_worldspace;

out vec4 vertex_position_worldspace;
out vec4 vertex_normal_worldspace;
out vec4 vertex_color_out;
out vec4 eye_direction;
out vec4 light_direction;

void main(void)
{
	// Compute real position of the object within the NDC
	mat4 mvp = projection_matrix * view_matrix * (model_matrix);
	gl_Position = vec4(mvp * vertex_position);

	// Compute world-space vertex data
	vertex_position_worldspace = model_matrix * vertex_position;
	vertex_normal_worldspace = transpose(inverse((model_matrix))) * vertex_normal;
	vertex_color_out = vertex_color;

	// Compute eye direction (camera is at 0,0,0,0) in camera space
	vec4 eye_position_cameraspace = ( view_matrix * model_matrix * vertex_position);
	eye_direction = vec4(0,0,0,0) - eye_position_cameraspace;

	// Compute light direction from the camera space
	vec4 light_position = ( view_matrix * light_position_worldspace);
	light_direction = light_position + eye_direction;
}
