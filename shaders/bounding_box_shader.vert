#version 410 core
#line 1
#extension GL_ARB_gpu_shader_fp64 : enable

layout (location = 0) in vec4 vertex_position;

uniform mat4 projection_matrix;
uniform mat4 view_matrix;
uniform mat4 model_matrix;
uniform vec4 light_position_worldspace;

out vec4 vertex_position_worldspace;

void main(void)
{
	// Compute real position of the object within the NDC
	mat4 mvp = projection_matrix * view_matrix * (model_matrix);
	gl_Position = vec4(mvp * vertex_position);

	// Compute world-space vertex data
	vertex_position_worldspace = model_matrix * vertex_position;
}
