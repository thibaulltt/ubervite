#include "../include/scene_object.hpp"

#include <algorithm>

#include "../include/scene_manager.hpp"

using namespace scene;

scene_object::scene_object(managers::scene_manager* node, scene::scene_object* node_parent) : manager(node), parent(node_parent) {
	this->name = "untitled_gameobject";
	this->drawable_instance = std::vector<drawable*>();
}

scene_object::scene_object(managers::scene_manager* manager, const aiScene* scene, const aiNode* node, scene_object* parent) :
	manager(manager), parent(parent) {
	this->drawable_instance = std::vector<drawable*>();
	// get the drawable instance, if needed :
	if (node->mNumMeshes > 0) {
		for (uint i = 0; i < node->mNumMeshes; ++i) {
			this->drawable_instance.push_back(manager->get_drawable_elements()[node->mMeshes[i]]);
		}
	} else {
		// TODO : Do something here, such as check against lights and camera with the name of the node
	}

	// Get the string for the name
	this->name = node->mName.C_Str();
	// Set the position of the object
	this->position = core::transform_t(core::mat4(node->mTransformation));

	std::cerr << "Currently on node " << this->name << " with " << node->mNumChildren << " children." << std::endl;

	// Propagate on all children
	for (uint i = 0; i < node->mNumChildren; ++i) {
		this->children.push_back(new scene_object(manager, scene, node->mChildren[i], this));
	}
}

std::pair<const drawable*, double> scene_object::check_raycast(std::pair<core::vec4, core::vec4> ray) const {
	// Here, the ray is supposed to be set in the parent's coordinate system, since we need to get in in our coordinate system
	ray.first = ray.first * this->position.get_inverse();
	ray.second = ray.second * this->position.get_inverse();

	// Get some default data :
	double min_dist_for_rays = std::numeric_limits<double>::max();
	drawable* closest_obj = nullptr;

	// Check for all drawable instances :
	for (const auto& d : this->drawable_instance) {
		std::pair<core::vec4, double> p = static_cast<mesh*>(d)->check_raycast_intersection(ray);
		if (p.second < min_dist_for_rays && p.second > .0) {
			min_dist_for_rays = p.second;
			closest_obj = d;
		}
	}

	// Check for all child scene objects :
	for (const auto& c : this->children) {
		std::pair<const drawable*, double> p = c->check_raycast(ray);
		if (p.second < min_dist_for_rays && p.second > .0) {
			min_dist_for_rays = p.second;
			closest_obj = const_cast<drawable*>(p.first);
		}
	}

	// Return closest match or nullptr if none :
	return std::make_pair(closest_obj, min_dist_for_rays);
}

void scene_object::add_instance(drawable *instance) {
	this->drawable_instance.push_back(instance);
}

void scene_object::clear_instances() {
	this->drawable_instance.clear();
}

void scene_object::render(const core::mat4 parent_position) {
	// Get current position from parent scene object
	core::mat4 model_matrix = parent_position * this->position.get_position();

	// If the current node CAN be rendered :
	for (const auto& d : this->drawable_instance) {
		// Render current model with supplied position :
		d->render_object(model_matrix);
	}

	// Propagate it to the children scene objects :
	for (const auto& child : this->children) {
		child->render(model_matrix);
	}

	return;
}

void scene_object::set_parent(scene_object* _parent) {
	// Remove the object from parent, if exists :
	if (this->parent != nullptr) {
		this->parent->remove_child(this);
	} else {
		this->manager->add_scene_object(this);
	}

	// Set new parent :
	this->parent = _parent;
}

void scene_object::add_child(scene_object *child) {
	if (child != nullptr) {
		std::vector<scene::scene_object*>::iterator pos = std::find(this->children.begin(), this->children.end(), child);
		if (pos == this->children.end()) {
			// if the child was not previously
			// set in child to this node
			this->children.push_back(child);
			child->set_parent(this);
		}
	}
}

void scene_object::add_children(std::vector<scene_object *> _children) {
	for (const auto& child : _children) {
		this->add_child(child);
	}
}

void scene_object::remove_child(scene_object *child) {
	const auto& pos = std::find(this->children.begin(), this->children.end(), child);
	if (pos != this->children.end()) {
		this->children.erase(pos);
	} else {
		std::cerr << "Error : object " << child->name << "was not a child of " <<
		this->name << ", yet the parent node of it was set to " << this->name << std::endl;
	}
}

scene::scene_object* scene_object::recursive_name_search(const std::string & query) const {
	if (this->name == query) {
		return const_cast<scene::scene_object*>(this);
	} else {
		if (this->children.size() == 0) return nullptr;
		scene::scene_object* child_res = nullptr;
		for (const auto& c : this->children) {
			child_res = c->recursive_name_search(query);
			if (child_res != nullptr) {
				return child_res;
			}
		}
		return nullptr;
	}
}

void scene_object::recursive_full_name_search(const std::string& query, std::vector<scene::scene_object*>& results) const {
	if (this->name == query) {
		results.push_back(const_cast<scene::scene_object*>(this));
	}
	if (this->children.size() == 0) return;
	scene::scene_object* child_res = nullptr;
	for (const auto& c : this->children) {
		child_res = c->recursive_name_search(query);
		if (child_res != nullptr) {
			results.push_back(child_res);
		}
	}
	return;
}

void scene_object::print_info(std::string &indent_level) const {
	// Virtual function : filled in by the daughter classes.
	std::cout << this->name << std::endl;
	for (const auto& child : children) {
		std::string newindent = indent_level + '\t';
		child->print_info(newindent);
	}
	return;
}
