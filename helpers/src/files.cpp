#include "../include/files.hpp"

#include <fcntl.h>

#include <iostream>
#include <fstream>
#include <sstream>

helpers::file_avail_t helpers::check_file_existence(const std::string &path_name) {
	FILE* concerned_file = fopen(path_name.c_str(), "rw");
	if (concerned_file == NULL) {
		concerned_file = fopen(path_name.c_str(), "r");
		if (concerned_file == NULL) {
			return file_avail_t::NOT_EXISTING;
		}
		fclose(concerned_file);
		return file_avail_t::READ_ONLY;
	}
	fclose(concerned_file);
	return file_avail_t::AVAILABLE;
}

std::string helpers::file_to_string(const std::string path_name) {
	// Read the file
	std::string file_contents;
	std::ifstream file_stream(path_name, std::ios::in);
	if (file_stream.is_open()) {
		std::stringstream sstr;
		sstr << file_stream.rdbuf();
		file_contents = sstr.str();
		file_stream.close();
		return file_contents;
	} else {
		std::cout << "Impossible to open " << path_name << ". Are you in the right directory ?" << std::endl;
		getchar();
		return "";
	}
}
