#include "../include/drawable_bounding_box.hpp"

#include "../../entities/include/generic_camera.hpp"

drawable_bounding_box::drawable_bounding_box(QOpenGLContext* context, const core::bounding_box& bb) : drawable(context) {
	this->corners.clear();

	for (size_t i = 0; i < 8; ++i) {
		this->corners.push_back(bb.corner(i));
	}
	/**
	 * WARNING : hacky code ahead. Used to draw bounding boxes, but apparently not good enough.
	 */
	this->vertex_shader_path = "../shaders/bounding_box_shader.vert";
	this->fragment_shader_path = "../shaders/bounding_box_shader.frag";

	this->line_strip.clear();

	this->line_strip.push_back(0);
	this->line_strip.push_back(1);

	this->line_strip.push_back(1);
	this->line_strip.push_back(5);

	this->line_strip.push_back(5);
	this->line_strip.push_back(4);

	this->line_strip.push_back(4);
	this->line_strip.push_back(6);

	this->line_strip.push_back(6);
	this->line_strip.push_back(2);

	this->line_strip.push_back(2);
	this->line_strip.push_back(0);

	this->line_strip.push_back(0);
	this->line_strip.push_back(1);

	this->line_strip.push_back(1);
	this->line_strip.push_back(3);

	this->line_strip.push_back(3);
	this->line_strip.push_back(2);

	this->line_strip.push_back(2);
	this->line_strip.push_back(6);

	this->line_strip.push_back(6);
	this->line_strip.push_back(7);

	this->line_strip.push_back(7);
	this->line_strip.push_back(3);

	this->line_strip.push_back(3);
	this->line_strip.push_back(1);

	this->line_strip.push_back(1);
	this->line_strip.push_back(5);

	this->line_strip.push_back(5);
	this->line_strip.push_back(7);

	this->line_strip.push_back(7);
	this->line_strip.push_back(0);

	this->line_strip.push_back(0);
	this->line_strip.push_back(2);
	this->line_strip.push_back(1);
	this->line_strip.push_back(3);
}

void drawable_bounding_box::init_gl() {
	// Setup basic attributes :
	this->set_draw_mode(GL_LINES);
	this->is_hidden = false;
	this->elements_to_draw = static_cast<GLsizei>(this->line_strip.size());

	// Load shaders :
	this->load_shaders();

	// Create VAO, and bind it
	if (this->vao.create()) {
		QOpenGLVertexArrayObject::Binder vaoBinder(&this->vao);

		// Create vertex buffer
		if (this->vertex_buffer.create()) {
			if (this->vertex_buffer.bind()) {
				this->vertex_buffer.allocate(this->corners.data(), static_cast<int>(this->corners.size() * sizeof(core::vec4)));
				this->vertex_buffer.setUsagePattern(QOpenGLBuffer::StaticDraw);
				this->vertex_buffer.release();
			} else {
				std::cerr << "Vertex buffer could not be bound. No data was allocated." << std::endl;
			}
		} else {
			std::cerr << "Vertex buffer could not be created. No data was allocated." << std::endl;
		}

		// Create face buffer
		if (this->face_buffer.create()) {
			if (this->face_buffer.bind()) {
				this->face_buffer.allocate(this->line_strip.data(), static_cast<int>(this->line_strip.size() * sizeof(uint32_t)));
				this->face_buffer.setUsagePattern(QOpenGLBuffer::StaticDraw);
				this->face_buffer.release();
			} else {
				std::cerr << "Face buffer could not be bound. No data was allocated." << std::endl;
			}
		} else {
			std::cerr << "Face buffer could not be created. No data was allocated." << std::endl;
		}

		this->setup_vertex_attribs();
	} else {
		std::cerr << "mesh : Could not create a VAO object. No OpenGL storage was allocated." << std::endl;
	}
}

void drawable_bounding_box::setup_vertex_attribs() {
	QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();

	this->vertex_buffer.bind();
	f->glEnableVertexAttribArray(0);
	f->glVertexAttribPointer(0, 4, GL_DOUBLE, GL_FALSE, 4*sizeof(double), nullptr);
	this->vertex_buffer.release();
}

void drawable_bounding_box::render_object(const core::mat4 model_matrix) {
	if (!QOpenGLContext::currentContext()->isValid()) {
		std::cerr << "Current opengl context is not valid" << std::endl;
	}
	if (!is_hidden) {
		// Bind the VAO object, to draw it :
		QOpenGLVertexArrayObject::Binder vaoBinder(&this->vao);
		if (!this->program->bind()) {
			std::cerr << "Program not bound, cannot draw." << std::endl;
			return;
		}

		int proj_loc = this->program->uniformLocation("projection_matrix");
		int view_loc = this->program->uniformLocation("view_matrix");
		int model_loc = this->program->uniformLocation("model_matrix");
		int light_loc = this->program->attributeLocation("light_position_worldspace");

		if (proj_loc > -1) { this->program->setUniformValue(proj_loc, QMatrix4x4(camera->get_projection_matrix().to_array_float())); } //
		if (view_loc > -1) { this->program->setUniformValue(view_loc, QMatrix4x4(camera->get_view_matrix().to_array_float())); }
		if (model_loc > -1) { this->program->setUniformValue(model_loc, QMatrix4x4(model_matrix.to_array_float())); }
		if (light_loc > -1) { this->program->setUniformValue(light_loc, QVector4D(-45.f, 5.f, -6.f,1.f)); }

		if (this->face_buffer.bind()) {
			QOpenGLContext::currentContext()->functions()->glDrawArrays(this->draw_mode, 0, this->elements_to_draw);
		}

		this->program->release();
	} else {
		std::cerr << "Could not render object, it was hidden" << std::endl;
	}
}

drawable_bounding_box::~drawable_bounding_box() {
	this->corners.clear();
	this->line_strip.clear();
}
