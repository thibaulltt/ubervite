#ifndef _HELPERS_INCLUDE_XML_HPP_
#define _HELPERS_INCLUDE_XML_HPP_

#include <vector>
#include <string>
#include <istream>
#include <fstream>

#include "./xml_types.hpp"
#include "./xml_attributes.hpp"
#include "./xml_scope.hpp"

namespace helpers {

	namespace xml {

		/**
		 * Cette classe définit un parseur XML très simplifié. Il
		 * n'adhère à aucun standard XML, il permet juste de retourner
		 * un arbre contenant tous les éléments XML de façon à charger
		 * une scène dans la classe scene_loader (définie dans le fichier
		 * loaders/include/scene_loader.hpp).
		 *
		 * ABANDONNED / DEPRECATED
		 */
		class parser {
			protected:
				/* The file to read, under a stream form. */
				std::ifstream file;
				/* The root of the hierarchy described by the XML file. 
				 * Usually, is above any tags, as there may be multiple
				 * "roots" in the file (think about how in HTML you have
				 * <head> and <body> "roots") */
				scope* root_scope;
			public:
				/* Constructor initialising a stream for reading. */
				parser(const std::string&);
				/* Pick a new file, and reset the parser's state. */
				void pick_file(const std::string&);
				/* Parse the current file, with the given constraints of our scene system. */
				scope* parse();
				/* Quickly scan the file, listing all available scene descriptions, if there are any. */
				std::vector<std::string> quick_scene_parse();
			protected:
				/* Tokenize the currently loaded file. */
				std::vector<token> tokenize_file();
				/* From those tokens, detect a scope */
				scope* detect_scope(std::vector<token>);
		};

	}

}

#endif // _HELPERS_INCLUDE_XML_HPP_
