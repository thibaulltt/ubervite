#ifndef _INCLUDE_CORE_HPP_
#define _INCLUDE_CORE_HPP_

#include <cmath>

#define CORE_EPSILON 1e-15
#define NEAR_ZERO(x) ((x < CORE_EPSILON) && (x > -CORE_EPSILON))

namespace core {
	const double _EPSILON = 1e-5;
	const double DegToRad = M_PI / 180.0;
	const double RadToDeg = 180.0 / M_PI;
}

#endif // _INCLUDE_CORE_HPP_
