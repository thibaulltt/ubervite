#ifndef _SCENE_INCLUDE_SCENE_OBJECT_HPP_
#define _SCENE_INCLUDE_SCENE_OBJECT_HPP_

#include "../../core/core_include_all.hpp"

#ifndef __gl_h_
#include <GL/gl.h>
#endif

#include "../../opengl/include/generic_drawable_object.hpp"
#include "../../opengl/include/drawable_mesh.hpp"

namespace managers {
	class scene_manager;
}

namespace scene {

	/* Base class for an object in a scene. */
	class scene_object {
		public:
			/**
			 * @brief Name of the scene object.
			 */
			std::string name;
			/**
			 * @brief Object's position from within it's reference frame
			 * @details Current object position. Also serves as a way to enter the object's coordinate space from it's parent's coordinate space, for tree traversal.
			 */
			core::transform_t position;
			/**
			 * @brief Scene manager the object is rattached to.
			 */
			managers::scene_manager* manager;
			/**
			 * @brief Child objects of this object.
			 */
			std::vector<scene_object*> children;
			/**
			 * @brief Pointer to parent object
			 */
			scene_object* parent;
			/**
			 * @brief A drawable mesh
			 */
			std::vector<drawable*> drawable_instance;
		public:
			/**
			 * @brief Empty contructor of scene object.
			 * @param manager The scene manager the object is tied to
			 * @param parent The parent node of the scene object
			 */
			scene_object(managers::scene_manager* manager, scene::scene_object* parent = nullptr);
			/**
			 * @brief Constructor for a scene object from ASSIMP
			 * @param manager The scene manager the object will be attached to
			 * @param scene The scene as loaded by ASSIMP
			 * @param node The node as loaded by ASSIMP
			 */
			scene_object(managers::scene_manager* manager, const aiScene* scene, const aiNode* node, scene_object* parent = nullptr);
			/**
			 * @brief Renders the current object with the view matrix provided by the camera
			 * @param parent_position The position of the parent object
			 */
			void render(const core::mat4 parent_position = core::mat4());
			/**
			 * @brief Sets the parent object for this object.
			 * @param parent The new parent object.
			 */
			void set_parent(scene_object* parent);
			/**
			 * @brief Set a new instance to be drawn, replacing the current one
			 * @param instance
			 */
			void add_instance(drawable* instance);
			/**
			 * @brief Remove instances
			 */
			void clear_instances();
			/**
			 * @brief Checks if the raycast in question intersects with the instance, if any are provided.
			 * @param ray The ray to check intersection for
			 * @return A pair of <closest_obj, distance>. Note that the closest object might be nullptr, if there are no intersections.
			 * @note The ray is assumed to be transformed in the parent node's coordinate system, leaving only one final coordinate change.
			 */
			std::pair<const drawable*, double> check_raycast(std::pair<core::vec4, core::vec4> ray) const;
			/**
			 * @brief Get a read-only access to the drawable instance for this scene object, if there are any
			 * @return Pointers to the drawable instances, if any
			 */
			const std::vector<drawable*> get_instance() const { return this->drawable_instance; }
			/**
			 * @brief Get the drawable instance for this scene object, if there are any
			 * @return A pointer to the drawable instance, if any
			 */
			std::vector<drawable*> get_instance() { return this->drawable_instance; }
			/**
			 * @brief Ads a child to the current object
			 * @param child The new child object
			 */
			void add_child(scene_object* child);
			/**
			 * @brief Calls add_child() multiple times, to insert the whole vector in child
			 * @param children The children of the node
			 */
			void add_children(std::vector<scene_object*> children);
			/**
			 * @brief Removes a child from the architecture
			 * @param child The child to remove
			 */
			void remove_child(scene_object* child);
			/**
			 * @brief Prints info about the current object
			 * @param indent_level Indentation level for tree printing
			 */
			void print_info(std::string& indent_level) const;
			/**
			 * @brief Searches a scene_object node with the provided name recursively, stopping at the first occurence.
			 */
			scene::scene_object* recursive_name_search(const std::string&) const;
			/**
			 * @brief Searches a scene_object node with the provided name recursively, neverending.
			 */
			void recursive_full_name_search(const std::string&, std::vector<scene::scene_object*>&) const;
			/**
			 * @brief Basic function to reflect an incoming ray. Will be used for interactible elements later
			 * @return A point and direction of the ray. If the direction's length is 0, then the ray was not reflected.
			 */
			std::pair<core::vec4, core::vec4> reflect() const;
	};

}

#endif // _SCENE_INCLUDE_SCENE_OBJECT_HPP_
