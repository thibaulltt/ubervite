#include "../include/inspectors.hpp"

#include <QHBoxLayout>
#include <QLabel>

#include "../include/glwidget.h"

transform_inspector::transform_inspector(core::transform_t* transform, std::string name, QWidget* parent) : QWidget(parent) {
	if (transform == nullptr) {
		transform = new core::transform_t();
	}
	// Create vector inputs
	this->transform_input = new custom_vector_input(transform->get_translation_vec4(), "Translation", this);
	this->rotation_input = new custom_vector_input(transform->get_rotation_vec4(), "Rotation", this);
	this->scale_input = new custom_vector_input(transform->get_scaling_vec4(), "Scaling", this);
	// Create a title label
	QLabel* title_label = new QLabel(std::string("Transform of "+name).c_str());
	title_label->setAlignment(Qt::AlignCenter | Qt::AlignVCenter);

	connect(this->transform_input, &custom_vector_input::value_changed, this, &transform_inspector::signal_passthrough);
	connect(this->rotation_input, &custom_vector_input::value_changed, this, &transform_inspector::signal_passthrough);
	connect(this->scale_input, &custom_vector_input::value_changed, this, &transform_inspector::signal_passthrough);

	// Align everything in a vertical layout
	QVBoxLayout* allWidgets = new QVBoxLayout(this);
	allWidgets->addWidget(title_label);
	allWidgets->addWidget(this->transform_input);
	allWidgets->addWidget(this->rotation_input);
	allWidgets->addWidget(this->scale_input);
	this->setLayout(allWidgets);

	this->ref = transform;
}

transform_inspector::transform_inspector(core::transform_t* transform, std::string name, camera_inspector* cam_insp) : QWidget(cam_insp) {
	if (transform == nullptr) {
		transform = new core::transform_t();
	}
	// Create vector inputs
	this->transform_input = new custom_vector_input(core::vec4(transform->get_translation()), "Translation", this);
	this->rotation_input= new custom_vector_input(core::vec4(transform->get_rotation()), "Rotation", this);
	this->scale_input = new custom_vector_input(core::vec4(transform->get_scaling()), "Scaling", this);
	// Create a title label
	QLabel* title_label = new QLabel(std::string("Transform of "+name).c_str());
	title_label->setAlignment(Qt::AlignCenter | Qt::AlignVCenter);

	connect(this->transform_input, &custom_vector_input::value_changed, this, &transform_inspector::signal_passthrough);
	connect(this->rotation_input, &custom_vector_input::value_changed, this, &transform_inspector::signal_passthrough);
	connect(this->scale_input, &custom_vector_input::value_changed, this, &transform_inspector::signal_passthrough);

	connect(this, &transform_inspector::any_value_changed, cam_insp, &camera_inspector::signal_passthrough);
	connect(cam_insp, &camera_inspector::glwidget_message_transform, this, &transform_inspector::update_internal_values);

	// Align everything in a vertical layout
	QVBoxLayout* allWidgets = new QVBoxLayout(this);
	allWidgets->addWidget(title_label);
	allWidgets->addWidget(this->transform_input);
	allWidgets->addWidget(this->rotation_input);
	allWidgets->addWidget(this->scale_input);
	this->setLayout(allWidgets);

	this->ref = transform;
}

void transform_inspector::signal_passthrough() {
	this->ref->set_translation(this->transform_input->get_value());
	this->ref->set_rotation(this->rotation_input->get_value());
	this->ref->set_scaling(this->scale_input->get_value());
	emit this->any_value_changed();
}

void transform_inspector::update_internal_values() {
	this->blockSignals(true);
	this->transform_input->update_field(camera->transform.get_translation_vec4());
	this->rotation_input->update_field(camera->transform.get_rotation_vec4());
	this->scale_input->update_field(camera->transform.get_scaling_vec4());
	this->blockSignals(false);
}

camera_projection_inspector::camera_projection_inspector(camera_inspector* cam_insp) {
	this->setParent(cam_insp);
	// Connect to the parent camera inspector :
	connect(this, &camera_projection_inspector::any_value_changed, cam_insp, &camera_inspector::signal_passthrough);
	// Connect the camera update signal to the update slot :
	connect(cam_insp, &camera_inspector::glwidget_message_projection, this, &camera_projection_inspector::update_internal_values);

	// Create all custom fields :
	this->left_input = new custom_float_input("Left : ", &camera->projection_parameters.left, this);
	this->right_input = new custom_float_input("Right : ", &camera->projection_parameters.right, this);
	this->bottom_input = new custom_float_input("Bottom : ", &camera->projection_parameters.bottom, this);
	this->top_input = new custom_float_input("Top : ", &camera->projection_parameters.top, this);
	this->near_input = new custom_float_input("Near : ", &camera->projection_parameters.near, this);
	this->far_input = new custom_float_input("Far : ", &camera->projection_parameters.far, this);
	this->custom_matrix_choice = new custom_checkbox_input("Use default identity matrix for projection", &camera->projection_parameters.use_default_identity_matrix, this);
	this->transpose_choice = new custom_checkbox_input("Transpose matrix ?", &camera->projection_parameters.transpose_matrix, this);
	this->info_debug_matrix_choice = new custom_checkbox_input("Print matrix info on resize ?", &camera->projection_parameters.print_info, this);

	// Connect their signals to the signal passthrough :
	connect(this->left_input, &custom_float_input::value_changed, this, &camera_projection_inspector::signal_passthrough);
	connect(this->right_input, &custom_float_input::value_changed, this, &camera_projection_inspector::signal_passthrough);
	connect(this->bottom_input, &custom_float_input::value_changed, this, &camera_projection_inspector::signal_passthrough);
	connect(this->top_input, &custom_float_input::value_changed, this, &camera_projection_inspector::signal_passthrough);
	connect(this->near_input, &custom_float_input::value_changed, this, &camera_projection_inspector::signal_passthrough);
	connect(this->far_input, &custom_float_input::value_changed, this, &camera_projection_inspector::signal_passthrough);
	connect(this->custom_matrix_choice, &custom_checkbox_input::value_changed, this, &camera_projection_inspector::signal_passthrough);
	connect(this->transpose_choice, &custom_checkbox_input::value_changed, this, &camera_projection_inspector::signal_passthrough);
	connect(this->info_debug_matrix_choice, &custom_checkbox_input::value_changed, this, &camera_projection_inspector::signal_passthrough);

	// Create a vertical box layout
	QVBoxLayout* allWidgetLayout = new QVBoxLayout(this);
	// Add them to the widget's vertical layout :
	allWidgetLayout->addWidget(this->left_input);
	allWidgetLayout->addWidget(this->right_input);
	allWidgetLayout->addWidget(this->bottom_input);
	allWidgetLayout->addWidget(this->top_input);
	allWidgetLayout->addWidget(this->near_input);
	allWidgetLayout->addWidget(this->far_input);
	allWidgetLayout->addWidget(this->custom_matrix_choice);
	allWidgetLayout->addWidget(this->transpose_choice);
	allWidgetLayout->addWidget(this->info_debug_matrix_choice);
	this->setLayout(allWidgetLayout);
}

void camera_projection_inspector::update_internal_values() {
	this->blockSignals(true);
	this->left_input->update_field(camera->projection_parameters.left);
	this->right_input->update_field(camera->projection_parameters.right);
	this->bottom_input->update_field(camera->projection_parameters.bottom);
	this->top_input->update_field(camera->projection_parameters.top);
	this->near_input->update_field(camera->projection_parameters.near);
	this->far_input->update_field(camera->projection_parameters.far);
	this->custom_matrix_choice->update_field(camera->projection_parameters.use_default_identity_matrix);
	this->transpose_choice->update_field(camera->projection_parameters.transpose_matrix);
	this->info_debug_matrix_choice->update_field(camera->projection_parameters.print_info);
	this->blockSignals(false);
}

void camera_projection_inspector::signal_passthrough() {
	camera->projection_parameters.left = this->left_input->get_value();
	camera->projection_parameters.right = this->right_input->get_value();
	camera->projection_parameters.bottom = this->bottom_input->get_value();
	camera->projection_parameters.top = this->top_input->get_value();
	camera->projection_parameters.near = this->near_input->get_value();
	camera->projection_parameters.far = this->far_input->get_value();
	camera->projection_parameters.use_default_identity_matrix = this->custom_matrix_choice->get_value();
	camera->projection_parameters.transpose_matrix = this->transpose_choice->get_value();
	camera->projection_parameters.print_info = this->info_debug_matrix_choice->get_value();
	emit this->any_value_changed();
}

camera_info_inspector::camera_info_inspector(camera_inspector* cam_insp) {
	this->setParent(cam_insp);
	this->ref = camera;

	// Create custom fields :
	this->camera_pos_x_input = new custom_float_input("X : ", &camera->transform.get_translation().x, this);
	this->camera_pos_y_input = new custom_float_input("Y : ", &camera->transform.get_translation().y, this);
	this->camera_pos_z_input = new custom_float_input("Z : ", &camera->transform.get_translation().z, this);
	this->camera_rho_input = new custom_float_input("Rho : ", &camera->rho, this);
	this->camera_theta_input = new custom_float_input("Theta : ", &camera->theta, this);
	this->camera_radius_input = new custom_float_input("Radius : ", &camera->radius, this);

	// Connect them to the corresponding slots :
	connect(this->camera_pos_x_input, &custom_float_input::value_changed, this, &camera_info_inspector::signal_passthrough);
	connect(this->camera_pos_y_input, &custom_float_input::value_changed, this, &camera_info_inspector::signal_passthrough);
	connect(this->camera_pos_z_input, &custom_float_input::value_changed, this, &camera_info_inspector::signal_passthrough);
	connect(this->camera_rho_input, &custom_float_input::value_changed, this, &camera_info_inspector::signal_passthrough);
	connect(this->camera_theta_input, &custom_float_input::value_changed, this, &camera_info_inspector::signal_passthrough);
	connect(this->camera_radius_input, &custom_float_input::value_changed, this, &camera_info_inspector::signal_passthrough);

	// Connect the camera inspector to the update fields of the inputs :
	connect(cam_insp, &camera_inspector::glwidget_message_view, this, &camera_info_inspector::update_internal_values);
	connect(this, &camera_info_inspector::any_value_changed, cam_insp, &camera_inspector::signal_passthrough);

	// Create a vertical box layout :
	QVBoxLayout* allWidgetLayout = new QVBoxLayout(this);
	allWidgetLayout->addWidget(this->camera_pos_x_input);
	allWidgetLayout->addWidget(this->camera_pos_y_input);
	allWidgetLayout->addWidget(this->camera_pos_z_input);
	allWidgetLayout->addWidget(this->camera_rho_input);
	allWidgetLayout->addWidget(this->camera_theta_input);
	allWidgetLayout->addWidget(this->camera_radius_input);
	this->setLayout(allWidgetLayout);
}

void camera_info_inspector::update_internal_values() {
	this->blockSignals(true);
	this->camera_pos_x_input->blockSignals(true);
	this->camera_pos_y_input->blockSignals(true);
	this->camera_pos_z_input->blockSignals(true);
	this->camera_rho_input->blockSignals(true);
	this->camera_theta_input->blockSignals(true);
	this->camera_radius_input->blockSignals(true);

	this->camera_pos_x_input->update_field(camera->transform.get_translation().x);
	this->camera_pos_y_input->update_field(camera->transform.get_translation().y);
	this->camera_pos_z_input->update_field(camera->transform.get_translation().z);
	this->camera_rho_input->update_field(camera->rho);
	this->camera_theta_input->update_field(camera->theta);
	this->camera_radius_input->update_field(camera->radius);

	this->camera_pos_x_input->blockSignals(false);
	this->camera_pos_y_input->blockSignals(false);
	this->camera_pos_z_input->blockSignals(false);
	this->camera_rho_input->blockSignals(false);
	this->camera_theta_input->blockSignals(false);
	this->camera_radius_input->blockSignals(false);
	this->blockSignals(false);
}

void camera_info_inspector::signal_passthrough() {
	this->ref->transform.set_translation(core::vec4(
		this->camera_pos_x_input->get_value(), this->camera_pos_y_input->get_value(),
		this->camera_pos_z_input->get_value(), .0)
	);
	this->ref->rho = this->camera_rho_input->get_value();
	this->ref->theta = this->camera_theta_input->get_value();
	this->ref->radius = this->camera_radius_input->get_value();
	emit this->any_value_changed();
}

camera_inspector::camera_inspector(entities::generic_camera* camera, GLWidget* glw) {
	this->camera_transform_input = new transform_inspector(&camera->transform, "Camera", this);
	this->camera_info_input = new camera_info_inspector(this);
	this->projection_input = new camera_projection_inspector(this);

	// Connect to glwidget
	connect(this, &camera_inspector::any_value_changed, glw, &GLWidget::camera_inspector_message);
	connect(glw, &GLWidget::camera_inspector_update_request, this, &camera_inspector::update_internal_values);
	QLabel* proj_label = new QLabel("Projection parameters");

	QVBoxLayout* allLayout = new QVBoxLayout(this);
	allLayout->addWidget(camera_transform_input);
	allLayout->addWidget(camera_info_input);
	allLayout->addWidget(proj_label);
	allLayout->addWidget(projection_input);
	this->setLayout(allLayout);
}

void camera_inspector::signal_passthrough() {
	emit this->any_value_changed();
}

void camera_inspector::update_internal_values() {
	emit this->glwidget_message_transform();
	emit this->glwidget_message_view();
	emit this->glwidget_message_projection();
}
