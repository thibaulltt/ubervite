#version 410 core
#line 1
#extension GL_ARB_gpu_shader_fp64 : enable

in vec4 vertex_position_worldspace;
in vec4 vertex_normal_worldspace;
in vec4 vertex_color_out;
in vec4 eye_direction;
in vec4 light_direction;

out vec4 color;

// TODO : add some uniform for lights
// TODO : add more than one light source (through GL_LIGHT<N> and GL_MAX_LIGHTS)

void main(void)
{
	// Normalize important vectors
	vec4 norm = normalize(vertex_normal_worldspace);
	vec4 light_dir = normalize(light_direction);

	double diff = max(dot(norm, light_dir), 0.0);
	vec4 ambient = vec4(1.,1.,1.,1.0);
	vec4 diffuse = vec4(diff * vec4(1.0,1.0,1.0,1.0));
	color = (ambient+diffuse) * vertex_color_out;
//	color = vec4(abs(vertex_normal_worldspace));
}
