#ifndef _ENTITIES_INCLUDE_FPS_CAMERA_HPP_
#define _ENTITIES_INCLUDE_FPS_CAMERA_HPP_

#include "../../core/core_include_all.hpp"
#include "generic_camera.hpp"

namespace entities {

	/**
	 * @brief The fps_camera class represents a very simple FPS-like camera implementation.
	 * @details The fps_camera represents a FPS camera like seen in the recent games, with
	 * 3 DOF. It allows for the user to fly around the map, no-clip style. A recent patch
	 * has made it stick to a fixed height for usability reasons.
	 */
	class fps_camera : public generic_camera {

		public:
			/**
			 * @brief Default constructor, places a camera at the specified position.
			 * @param position The position of the camera. Defaults to the origin.
			 */
			fps_camera(core::vec4 position) : generic_camera(position) {}
			/**
			 * @brief Gets a raycast from the camera to the worldspace.
			 * @return A ray, with it's position and direction.
			 */
			virtual std::pair<core::vec4, core::vec4> get_raycast_worldspace() const override;
			/**
			 * @brief Updates the view matrix
			 */
			virtual void update_view_matrix() override;
			/**
			 * @brief Rotate the camera around the rho axis by `angle` degrees
			 * @param angle The amount of degrees the camera needs to rotate by
			 */
			virtual void rotate_rho(double angle) override;
			/**
			 * @brief Rotate the camera around the theta axis by `angle` degrees
			 * @param angle The amount of degrees the camera needs to rotate by
			 */
			virtual void rotate_theta(double angle) override;
			/**
			 * @brief Zoom in by amount
			 * @param amount
			 */
			virtual void zoom_in(double amount = .1) override;
			/**
			 * @brief Zoom out by amount
			 * @param amount
			 */
			virtual void zoom_out(double amount = .1) override;
			/**
			 * @brief Move the camera backwards, along it's forward vector.
			 * @param scalar The amount to move along the vector (refreshed every 1/60 seconds, need to be low)
			 */
			virtual void move_backwards(double scalar = .1) override;
			/**
			 * @brief Move the camera forward, along it's forward vector.
			 * @param scalar The amount to move along the vector (refreshed every 1/60 seconds, need to be low)
			 */
			virtual void move_forward(double scalar = .1) override;
			/**
			 * @brief Move the camera left, along it's right vector.
			 * @param scalar The amount to move along the vector (refreshed every 1/60 seconds, need to be low)
			 */
			virtual void move_left(double scalar = .1) override;
			/**
			 * @brief Move the camera right, along it's right vector.
			 * @param scalar The amount to move along the vector (refreshed every 1/60 seconds, need to be low)
			 */
			virtual void move_right(double scalar = .1) override;
			/**
			 * @brief Move the camera down, along it's up vector.
			 * @param scalar The amount to move along the vector (refreshed every 1/60 seconds, need to be low)
			 */
			virtual void move_down(double scalar = .1) override;
			/**
			 * @brief Move the camera up, along it's up vector.
			 * @param scalar The amount to move along the vector (refreshed every 1/60 seconds, need to be low)
			 */
			virtual void move_up(double scalar = .1) override;
		protected:
			/**
			 * @brief Computes the heading vector
			 * @return The heading vector in world-space
			 */
			virtual core::vec4 compute_heading_vector() const override;
			/**
			 * @brief Compute the vector in the 'forward' direction of the camera.
			 * @return The forward vector of the camera
			 */
			virtual core::vec4 compute_forward_vector() const;
			/**
			 * @brief Compute the vector in the 'right' direction of the camera.
			 * @return The right vector of the camera
			 */
			virtual core::vec4 compute_right_vector() const;
			/**
			 * @brief Computes the up vector of the camera
			 * @return The up vector of the camera
			 */
			virtual core::vec4 compute_up_vector() const;
	};
}

#endif // _ENTITIES_INCLUDE_FPS_CAMERA_HPP_
