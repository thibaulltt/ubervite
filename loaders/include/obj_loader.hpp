#ifndef _LOADERS_INCLUDE_OBJ_LOADER_HPP_
#define _LOADERS_INCLUDE_OBJ_LOADER_HPP_

#include <vector>
#include <assimp/scene.h>

#include <QOpenGLContext>

#include "../../core/core_include_all.hpp"
#include "../../scene/include/scene_manager.hpp"

// TODO : Add some light addition into the parsing : complete the get_light_info function

managers::scene_manager* parseObjFile(QOpenGLContext* context, const char* path_name);

core::mat4 get_light_info(const aiLight* light);

#endif // _LOADERS_INCLUDE_OBJ_LOADER_HPP_
