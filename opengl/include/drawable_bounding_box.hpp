#ifndef DRAWABLE_BOUNDING_BOX_HPP
#define DRAWABLE_BOUNDING_BOX_HPP

#include "./generic_drawable_object.hpp"

/**
 * @brief The drawable_bounding_box class represents a drawable bounding box
 * @note It is a quick hack only to see where the bounds of an object start and end, for debug purposes. Not intended for final use.
 */
class drawable_bounding_box : public drawable {
	protected:
		std::vector<core::vec4> corners;
		std::vector<uint32_t> line_strip;
	public:
		/**
		 * @brief Constructs a drawable bounding box
		 * @param context The context to draw in
		 * @param bb The bounding box to draw from
		 */
		drawable_bounding_box(QOpenGLContext* context, const core::bounding_box& bb);
		/**
		 * @brief Initializes the OpenGL VAO/VBO buffers, and sets up the necessary elements to draw the mesh.
		 */
		virtual void init_gl() override;
		/**
		 * @brief Sets up the VAO object.
		 */
		virtual void setup_vertex_attribs() override;
		/**
		 * @brief Render the object to the screen
		 * @param model_matrix The parent model matrix
		 */
		virtual void render_object(const core::mat4 model_matrix) override;
		/**
		 * @brief Destroys the mesh
		 */
		virtual ~drawable_bounding_box() override;
};

#endif // DRAWABLE_BOUNDING_BOX_HPP
