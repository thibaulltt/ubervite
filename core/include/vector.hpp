/**==========================================
 * FILE : vector.hpp
 * AUTH : Thibault de VILLÈLE
 * DATE : 18/10/2019
 * DESC : Contient les définitions pour les
 *        points, vecteurs, et autres objets
 *        géométriques en 2 et 3 dimensions.
 * ==========================================
 */

#ifndef _CORE_INCLUDE_VECTOR_HPP_
#define _CORE_INCLUDE_VECTOR_HPP_

#include <iostream>
#include "core.hpp"

typedef int	GLsizei;
typedef float	GLfloat;

namespace core {

	class vec2 {

		public:
			/**
			 * Coordonnée X du vecteur
			 */
			double x;
			/**
			 * Coordonnée Y du vecteur
			 */
			double y;

		/**
		 * Fonctions internes au vecteurs
		 */
		public:
			/**
			 * Constructeur par défaut. Ne prends aucun argument et retourne un vecteur nul.
			 */
			vec2();
			/**
			 * Constructeur avec arguments. Permet de définir un vecteur avec les coordonnées (_x,_y)
			 */
			vec2(double _x, double _y);
			/**
			 * Constructeur par copie. Permet de copier un vecteur dans un nouveau.
			 */
			vec2(const vec2&);
			/**
			 * Constructeur par copie. Permet de copier un vecteur dans un nouveau.
			 */
			vec2(vec2&&);
			/**
			 * @brief Dot product between this vector and v
			 * @param v The vector to compute dot product against
			 * @return The dot product of `this` and `v`
			 * @see abs_dot()
			 */
			double dot(const vec2& v) const;
			/**
			 * @brief Absolute value of the dot product between this vector and v
			 * @param v The vector to compute dot product against
			 * @return The dot product of `this` and `v`
			 * @see dot()
			 */
			double abs_dot(const vec2& v) const;
			/**
			 * @brief Computes the length of the vector
			 * @return The length of the vector
			 * @see squared_length()
			 */
			double length() const;
			/**
			 * @brief Computes the squared length of the vector
			 * @return The squared length of the vector
			 * @see length()
			 */
			double squared_length() const;
			/**
			 * @brief Returns a normalized copy of this vector
			 * @return A normalized copy of this vector
			 * @see normalize_self()
			 */
			vec2 normalize() const;
			/**
			 * @brief Normalizes this vector, modifying it's coordinates.
			 * @return This vector, normalized
			 * @see normalize()
			 */
			vec2& normalize_self();
			/**
			 * @brief Permets un accès par crochets aux coordonnées du vecteur, par référence
			 */
			double& operator[](int i);
			/**
			 * @brief Permets un accès EN LECTURE par crochets aux coordonnées du vecteur, par référence
			 */
			const double operator[](int i) const;
			/**
			 * @brief Inverse le vecteur. Retourne un nouveau vecteur.
			 */
			vec2 operator- () const;
			/**
			 * Opérateur d'assignation. Modifie le vecteur courant.
			 */
			vec2& operator= (const vec2&);
			/**
			 * Additionne deux vecteurs entre eux. Retourne un nouveau vecteur.
			 */
			vec2 operator+ (const vec2&) const;
			/**
			 * Soustracte deux vecteurs entre eux. Retourne un nouveau vecteur.
			 */
			vec2 operator- (const vec2&) const;
			/**
			 * Multiplication par un scalaire. Retourne un nouveau vecteur.
			 */
			vec2 operator* (const double) const;
			/**
			 * Multiplication de vecteurs, éléments par éléments. Retourne un nouveau vecteur.
			 */
			vec2 operator* (const vec2&) const;
			/**
			 * Division par un scalaire des éléments du vecteur. Retourne un nouveau vecteur.
			 */
			vec2 operator/ (const double) const;
			/**
			 * Addition de vecteurs. Modifie l'objet courant.
			 */
			vec2& operator+=(const vec2&);
			/**
			 * Soustraction de vecteurs. Modifie l'objet courant.
			 */
			vec2& operator-=(const vec2&);
			/**
			 * Multiplication élément par élément avec un autre vecteur. Modifie l'objet courant.
			 */
			vec2& operator*=(const vec2&);
			/**
			 * Multiplication par un scalaire. Modifie l'objet courant.
			 */
			vec2& operator*=(const double);
			/**
			 * Divison par un scalaire. Modifie l'objet courant.
			 */
			vec2& operator/=(const double);
			/**
			 * Opérateur d'extraction pour affichage.
			 */
			friend std::ostream& operator<<(std::ostream& os, const vec2& v) {
				return os << "(" << v.x << "," << v.y << ")";
			}
			/**
			 * Opérateur d'insertion pour le remplissage de valeurs
			 */
			friend std::istream& operator>>(std::istream& is, vec2& v) {
				return is >> v.x >> v.y;
			}
	}; // end of "class vec2"

	class vec3 {

		public:
			/**
			 * Coordonnée X du vecteur
			 */
			double x;
			/**
			 * Coordonnée Y du vecteur
			 */
			double y;
			/**
			 * Coordonnée Z du vecteur
			 */
			double z;

		/**
		 * Fonctions internes au vecteurs
		 */
		public:
			/**
			 * Constructeur par défaut. Ne prends aucun argument et retourne un vecteur nul.
			 */
			vec3();
			/**
			 * Constructeur avec arguments. Permet de définir un vecteur avec les coordonnées (_x,_y,_z)
			 */
			vec3(double _x, double _y, double _z);
			/**
			 * Constructeur par copie. Permet de copier un vecteur dans un nouveau.
			 */
			vec3(const vec3&);
			/**
			 * Constructeur par copie. Permet de copier un vecteur bidimensionnel dans un vecteur tridimensionnel.
			 */
			vec3(const vec2&);
			/**
			 * Constructeur par copie. Permet de copier un vecteur dans un nouveau.
			 */
			vec3(vec3&&);
			/**
			 * @brief Dot product between this vector and v
			 * @param v The vector to compute dot product against
			 * @return The dot product of `this` and `v`
			 * @see abs_dot()
			 */
			double dot(const vec3& v) const;
			/**
			 * @brief Absolute value of the dot product between this vector and v
			 * @param v The vector to compute dot product against
			 * @return The dot product of `this` and `v`
			 * @see dot()
			 */
			double abs_dot(const vec3& v) const;
			/**
			 * @brief Compute the cross product of this vector and v
			 * @param v The vector to compute cross product for
			 * @return The vector created by the cross product of `this` and `v`
			 */
			vec3 cross(const vec3& v) const;
			/**
			 * @brief Computes the length of the vector
			 * @return The length of the vector
			 * @see squared_length()
			 */
			double length() const;
			/**
			 * @brief Computes the squared length of the vector
			 * @return The squared length of the vector
			 * @see length()
			 */
			double squared_length() const;
			/**
			 * @brief Returns a normalized copy of this vector
			 * @return A normalized copy of this vector
			 * @see normalize_self()
			 */
			vec3 normalize() const;
			/**
			 * @brief Normalizes this vector, modifying it's coordinates.
			 * @return This vector, normalized
			 * @see normalize()
			 */
			vec3& normalize_self();
			/**
			 * Permets un accès par crochets aux coordonnées du vecteur, par référence
			 */
			double& operator[](int i);
			/**
			 * Permets un accès EN LECTURE par crochets aux coordonnées du vecteur, par référence
			 */
			const double operator[](int i) const;
			/**
			 * Inverse le vecteur. Retourne un nouveau vecteur.
			 */
			vec3 operator- () const;
			/**
			 * Opérateur d'assignation. Modifie le vecteur courant.
			 */
			vec3& operator= (const vec3&);
			/**
			 * Additionne deux vecteurs entre eux. Retourne un nouveau vecteur.
			 */
			vec3 operator+ (const vec3&) const;
			/**
			 * Soustracte deux vecteurs entre eux. Retourne un nouveau vecteur.
			 */
			vec3 operator- (const vec3&) const;
			/**
			 * Multiplication par un scalaire. Retourne un nouveau vecteur.
			 */
			vec3 operator* (const double) const;
			/**
			 * Multiplication de vecteurs, éléments par éléments. Retourne un nouveau vecteur.
			 */
			vec3 operator* (const vec3&) const;
			/**
			 * Division par un scalaire des éléments du vecteur. Retourne un nouveau vecteur.
			 */
			vec3 operator/ (const double) const;
			/**
			 * Addition de vecteurs. Modifie l'objet courant.
			 */
			vec3& operator+=(const vec3&);
			/**
			 * Soustraction de vecteurs. Modifie l'objet courant.
			 */
			vec3& operator-=(const vec3&);
			/**
			 * Multiplication élément par élément avec un autre vecteur. Modifie l'objet courant.
			 */
			vec3& operator*=(const vec3&);
			/**
			 * Multiplication par un scalaire. Modifie l'objet courant.
			 */
			vec3& operator*=(const double);
			/**
			 * Divison par un scalaire. Modifie l'objet courant.
			 */
			vec3& operator/=(const double);
			/**
			 * Opérateur d'extraction pour affichage.
			 */
			friend std::ostream& operator<<(std::ostream& os, const vec3& v) {
				return os << "(" << v.x << "," << v.y << "," << v.z << ")";
			}
			/**
			 * Opérateur d'insertion pour le remplissage de valeurs
			 */
			friend inline std::istream& operator>>(std::istream& is, vec3& v) {
				is >> v.x >> v.y >> v.z;
				return is;
			}
	}; // end of "class vec3"

	class vec4 {

		public:
			/**
			 * Coordonnée X du vecteur
			 */
			double x;
			/**
			 * Coordonnée Y du vecteur
			 */
			double y;
			/**
			 * Coordonnée Z du vecteur
			 */
			double z;
			/**
			 * Coordonnée omega du vecteur
			 */
			double w;

		/**
		 * Fonctions internes au vecteurs
		 */
		public:
			/**
			 * Constructeur par défaut. Ne prends aucun argument et retourne un vecteur nul.
			 */
			vec4();
			/**
			 * Constructeur avec arguments. Permet de définir un vecteur avec les coordonnées (_x,_y,_z)
			 */
			vec4(double _x, double _y, double _z, double _w);
			/**
			 * Constructeur par copie. Permet de copier un vecteur dans un nouveau.
			 */
			vec4(const vec4&);
			/**
			 * Constructeur par copie. Permet de copier un vecteur tridimensionnel dans un vecteur quadridimensionnel.
			 */
			vec4(const vec3&);
			/**
			 * Constructeur par copie. Permet de copier un vecteur bidimensionnel dans un vecteur quadridimensionnel.
			 */
			vec4(const vec2&);
			/**
			 * Constructeur par copie. Permet de copier un vecteur dans un nouveau.
			 */
			vec4(vec4&&);
			/**
			 * @brief Dot product between this vector and v
			 * @param v The vector to compute dot product against
			 * @return The dot product of `this` and `v`
			 * @see abs_dot()
			 */
			double dot(const vec4& v) const;
			/**
			 * @brief Absolute value of the dot product between this vector and v
			 * @param v The vector to compute dot product against
			 * @return The dot product of `this` and `v`
			 * @see dot()
			 */
			double abs_dot(const vec4& v) const;
			/**
			 * @brief Compute the cross product of this vector and v, only in three dimensions
			 * @param v The vector to compute cross product for
			 * @return The vector created by the cross product of `this` and `v`
			 * @warning The cross product here is done in 3 dimensions, whilst keeping the fourth dimension of the vector intact.
			 */
			vec4 cross(const vec4& v) const;
			/**
			 * @brief Computes the length of the vector
			 * @return The length of the vector
			 * @see squared_length()
			 */
			double length() const;
			/**
			 * @brief Computes the squared length of the vector
			 * @return The squared length of the vector
			 * @see length()
			 */
			double squared_length() const;
			/**
			 * @brief Returns a normalized copy of this vector
			 * @return A normalized copy of this vector
			 * @see normalize_self()
			 */
			vec4 normalize() const;
			/**
			 * @brief Normalizes this vector, modifying it's coordinates.
			 * @return This vector, normalized
			 * @see normalize()
			 */
			vec4& normalize_self();
			/**
			 * @brief Returns a GLfloat array, for lights
			 * @return
			 */
			GLfloat* to_glfloat_array() const;
			/**
			 * Permets un accès par crochets aux coordonnées du vecteur, par référence
			 */
			double& operator[](int i);
			/**
			 * Permets un accès EN LECTURE par crochets aux coordonnées du vecteur, par référence
			 */
			const double operator[](int i) const;
			/**
			 * Inverse le vecteur. Retourne un nouveau vecteur.
			 */
			vec4 operator- () const;
			/**
			 * Opérateur d'assignation. Modifie le vecteur courant.
			 */
			vec4& operator= (const vec4&);
			/**
			 * Additionne deux vecteurs entre eux. Retourne un nouveau vecteur.
			 */
			vec4 operator+ (const vec4&) const;
			/**
			 * Soustracte deux vecteurs entre eux. Retourne un nouveau vecteur.
			 */
			vec4 operator- (const vec4&) const;
			/**
			 * Multiplication par un scalaire. Retourne un nouveau vecteur.
			 */
			vec4 operator* (const double) const;
			/**
			 * Multiplication de vecteurs, éléments par éléments. Retourne un nouveau vecteur.
			 */
			vec4 operator* (const vec4&) const;
			/**
			 * Division par un scalaire des éléments du vecteur. Retourne un nouveau vecteur.
			 */
			vec4 operator/ (const double) const;
			/**
			 * Addition de vecteurs. Modifie l'objet courant.
			 */
			vec4& operator+=(const vec4&);
			/**
			 * Soustraction de vecteurs. Modifie l'objet courant.
			 */
			vec4& operator-=(const vec4&);
			/**
			 * Multiplication élément par élément avec un autre vecteur. Modifie l'objet courant.
			 */
			vec4& operator*=(const vec4&);
			/**
			 * Multiplication par un scalaire. Modifie l'objet courant.
			 */
			vec4& operator*=(const double);
			/**
			 * Divison par un scalaire. Modifie l'objet courant.
			 */
			vec4& operator/=(const double);
			/**
			 * Opérateur d'extraction pour affichage.
			 */
			friend std::ostream& operator<<(std::ostream& os, const vec4& v) {
				return os << "(" << v.x << "," << v.y << "," << v.z << "," << v.w << ")";
			}
			/**
			 * Opérateur d'insertion pour le remplissage de valeurs
			 */
			friend std::istream& operator>>(std::istream& is, vec4& v) {
				return is >> v.x >> v.y >> v.z >> v.w;
			}
	}; // end of "class vec4"

	typedef vec2 point2;
	typedef vec3 point3;
	typedef vec4 point4;

} // end of namespace core

#endif // _CORE_INCLUDE_VECTOR_HPP_
// vim:filetype=cpp:foldmethod=marker:foldmarker={{{,}}}:tabstop=8:
