#ifndef _SCENE_INCLUDE_SCENE_MANAGER_HPP_
#define _SCENE_INCLUDE_SCENE_MANAGER_HPP_

#include <vector>

#include "scene_object.hpp"

namespace entities { class generic_camera; class simple_mesh; }

// TODO : add some fucking lights for god's sake

namespace managers {

	class scene_manager {
		protected:
			/**
			 * @brief A vector of root game objects.
			 */
			std::vector<scene::scene_object*> root_objects;
			/**
			 * @brief Vector of pointers to lights
			 * @warning Even though the lights here are represented by a mat4,
			 * they do not indicate a TRS transform. They just indicate all
			 * the necessary information about the light in a compact manner.
			 */
			std::vector<core::mat4> lights;
			/**
			 * @brief Elements destined to be drawn on screen
			 */
			std::vector<drawable*> drawable_elements;
			/**
			 * @brief Vector of pointers to interactible elements
			 */
			std::vector<scene::scene_object*> interactible_elements;
			/**
			 * @brief Singular pointer to the camera
			 */
			entities::generic_camera* camera;
		public:
			/**
			 * @brief Default constructor for the scene manager. Initializes everything to 0 or nullptr.
			 */
			scene_manager();
			/**
			 * @brief Set the lights in the scene
			 * @param new_lights Lights to set as the lighting environment in the map
			 */
			void set_lights(std::vector<core::mat4> new_lights);
			/**
			 * @brief Sets the drawable elements in the manager.
			 * @param new_elements The elements to be drawn
			 */
			void set_drawable(std::vector<drawable*> new_elements);
			/**
			 * @brief Get the lights
			 * @return The lights
			 */
			const std::vector<core::mat4>& get_lights() const;
			/**
			 * @brief Get the vector of drawable elements
			 * @return The vector of drawable elements
			 */
			const std::vector<drawable*>& get_drawable_elements() const;
			/**
			 * @brief Add the scene object to the root of the tree.
			 * @param to_add The scene object to add.
			 */
			void add_scene_object(scene::scene_object* to_add);
			/**
			 * @brief Adds all the scene objects in this vector to the root of the tree.
			 * @param to_add The scene objects to add.
			 */
			void add_scene_objects(std::vector<scene::scene_object*> to_add);
			/**
			 * @brief Sets the root objects of the scene, discarding the previous ones.
			 * @param to_add The new objects to be at the root of the tree.
			 */
			void set_root_objects(std::vector<scene::scene_object*> to_add);
			/**
			 * @brief Searches a game object by name. Returns the first result in a depth-first recursive search.
			 * @param query The queried game object
			 * @return A reference to the scene object, and nullptr if none were found
			 * @note Does not support regex.
			 */
			scene::scene_object* search_by_string(const std::string query) const;
			/**
			 * @brief Searches all the tree for a particular search term, using a depth-first algorithm.
			 * @param query The term to search for
			 * @return A vector of references to scene objects named as the query implies.
			 * @note Does not support regex.
			 */
			std::vector<scene::scene_object*> search_all_by_string(std::string query) const;
			/**
			 * @brief Get the camera in the scene.
			 * @return A reference to the camera in the scene hierarchy
			 */
			const entities::generic_camera* get_camera() const;
			/**
			 * @brief Request to call all init_gl functions for each loaded mesh
			 */
			void request_object_initialization() const;
			/**
			 * @brief Gets the nearest intersected object, if there are any
			 * @param ray The ray to check intersections for.
			 * @return The scene object intersected, and nullptr if there are no intersections.
			 */
			drawable* get_raycast_intersection(const std::pair<core::vec4, core::vec4>& ray) const;
			/**
			 * @brief Request a render of each of the drawn elements on screen, at their coordinates
			 */
			void render() const;
			/**
			 * @brief Print info about the root objects
			 */
			void print_root_objects() const;
	};

}

#endif // _SCENE_INCLUDE_SCENE_MANAGER_HPP_
