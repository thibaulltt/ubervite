#include "../include/input_fields.hpp"

#include <QHBoxLayout>
#include <QLabel>

custom_float_input::custom_float_input(std::string name, const double* value, QWidget* parent) : QWidget(parent) {
	// Create the input box :
	this->input = new float_input_box((*value), this);
	connect(this->input, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &custom_float_input::signal_passthrough);
	QLabel* double_input_label = new QLabel(name.c_str(), this);

	// Put all elements in a horizontal layout box
	QHBoxLayout* layout_double_field = new QHBoxLayout(this);
	layout_double_field->addWidget(double_input_label);
	layout_double_field->addWidget(this->input);
	this->setLayout(layout_double_field);

	this->layout()->setMargin(0);
}

void custom_float_input::update_field(double val) {
	// Triggered from an outside signal, to update current value :
	this->input->setValue(val);
}

void custom_float_input::signal_passthrough() {
	// Set the new value :
	emit this->value_changed();
}

custom_checkbox_input::custom_checkbox_input(std::string name, bool* value, QWidget* parent) : QWidget(parent) {
	// Create a checkbox, and connect its signals to the current slot
	this->checkbox = new QCheckBox(this);
	this->checkbox->setCheckState(*value ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
	connect(this->checkbox, &QCheckBox::stateChanged, this, &custom_checkbox_input::signal_passthrough);
	QLabel* check_input = new QLabel(name.c_str());
	check_input->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

	// Add them in a horizontal layout :
	QHBoxLayout* layout_bool_field = new QHBoxLayout(nullptr);
	layout_bool_field->addWidget(this->checkbox);
	layout_bool_field->addWidget(check_input);
	this->setLayout(layout_bool_field);

	this->layout()->setMargin(0);
}

void custom_checkbox_input::update_field(bool cs) {
	// Triggered from an outside signal, to update current value :
	this->checkbox->setCheckState(cs ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
}

void custom_checkbox_input::signal_passthrough() {
	// Set the new value :
	emit this->value_changed();
}

custom_vector_input::custom_vector_input(core::vec4 values, std::string name, QWidget* parent) : QWidget(parent) {
	// Create label
	QLabel* vector_name = new QLabel(name.c_str());
	vector_name->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
	// Create input boxes
	this->x_input = new float_input_box(values.x, this);
	this->y_input =	new float_input_box(values.y, this);
	this->z_input =	new float_input_box(values.z, this);
	this->w_input =	new float_input_box(values.w, this);
	// Connect the signals to the signal passthrough
	connect(this->x_input, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &custom_vector_input::signal_passthrough);
	connect(this->y_input, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &custom_vector_input::signal_passthrough);
	connect(this->z_input, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &custom_vector_input::signal_passthrough);
	connect(this->w_input, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &custom_vector_input::signal_passthrough);

	// Lay them out horizontally :
	QHBoxLayout* input_layout = new QHBoxLayout(this);
	input_layout->addWidget(vector_name);
	input_layout->addWidget(this->x_input);
	input_layout->addWidget(this->y_input);
	input_layout->addWidget(this->z_input);
	input_layout->addWidget(this->w_input);
	this->setLayout(input_layout);

	this->layout()->setMargin(0);
}

void custom_vector_input::update_field(core::vec4 v) {
	this->blockSignals(true);
	this->x_input->blockSignals(true);
	this->y_input->blockSignals(true);
	this->z_input->blockSignals(true);
	this->w_input->blockSignals(true);

	this->x_input->setValue(v.x);
	this->y_input->setValue(v.y);
	this->z_input->setValue(v.z);
	this->w_input->setValue(v.w);

	this->x_input->blockSignals(false);
	this->y_input->blockSignals(false);
	this->z_input->blockSignals(false);
	this->w_input->blockSignals(false);
	this->blockSignals(false);
}

core::vec4 custom_vector_input::get_value() {
	return core::vec4(this->x_input->value(), this->y_input->value(), this->z_input->value(), this->w_input->value());
}

void custom_vector_input::signal_passthrough() {
	emit this->value_changed();
}
