/**==========================================
 * FILE : helpers/include/files.hpp
 * AUTH : Thibault de VILLÈLE
 * DATE : 30/10/2019
 * DESC : Contient des fonctions utiles au programme,
 * 	  notamment pour l'I/O des fichiers
 * ==========================================
 */

#ifndef _HELPERS_INCLUDE_FILES_HPP_
#define _HELPERS_INCLUDE_FILES_HPP_

#include <string>

namespace helpers {

	typedef enum file_availability_type {
		NOT_EXISTING,
		READ_ONLY,
		AVAILABLE
	} file_avail_t;

	// checks a file's existence on the system.
	file_avail_t check_file_existence(const std::string& path_name);

	std::string file_to_string(const std::string path_name);

}

#endif // _HELPERS_INCLUDE_FILES_HPP_
