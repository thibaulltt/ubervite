#include "../include/fps_camera.hpp"

#include <glm/common.hpp>
#include <glm/gtc/matrix_transform.hpp>

std::pair<core::vec4, core::vec4> entities::fps_camera::get_raycast_worldspace() const {
	return std::make_pair(this->transform.get_translation_vec4(), this->compute_forward_vector());
}

void entities::fps_camera::rotate_rho(double angle) {
	this->transform.rotate_z(angle);
}

void entities::fps_camera::rotate_theta(double angle) {
	this->transform.rotate_y(angle);
}

void entities::fps_camera::zoom_in(double amount) {
	this->radius += amount;
}

void entities::fps_camera::zoom_out(double amount) {
	this->radius -= amount;
}

void entities::fps_camera::update_view_matrix() {
	core::vec4 pos = this->transform.get_translation_vec4() + core::vec4(this->radius,.0,.0,.0) * this->transform.get_inverse(); // world space
	core::vec4 to = pos + (this->compute_forward_vector()); // world space
	core::vec4 up = this->compute_up_vector(); // world space
	this->view_matrix = core::mat4(glm::lookAt(glm::vec3(pos.x, pos.y, pos.z), glm::vec3(to.x, to.y, to.z), glm::vec3(up.x, up.y, up.z)));
}

void entities::fps_camera::move_backwards(double scalar) {
	core::vec4 dir = this->compute_forward_vector() *-scalar;
	this->transform.translate(dir.x, dir.y, dir.z);
	// Quick hack to get the camera to stay on the ground :
	core::vec4 pos = this->transform.get_translation_vec4();
	pos.y = this->heightmap_height;
	this->transform.set_translation(pos);
}

void entities::fps_camera::move_forward(double scalar) {
	core::vec4 dir = this->compute_forward_vector() * scalar;
	this->transform.translate(dir.x, dir.y, dir.z);
	// Quick hack to get the camera to stay on the ground :
	core::vec4 pos = this->transform.get_translation_vec4();
	pos.y = this->heightmap_height;
	this->transform.set_translation(pos);
}

void entities::fps_camera::move_left(double scalar) {
	core::vec4 dir = this->compute_right_vector() *-scalar;
	this->transform.translate(dir.x, dir.y, dir.z);
	// Quick hack to get the camera to stay on the ground :
	core::vec4 pos = this->transform.get_translation_vec4();
	pos.y = this->heightmap_height;
	this->transform.set_translation(pos);
}

void entities::fps_camera::move_right(double scalar) {
	core::vec4 dir = this->compute_right_vector() * scalar;
	this->transform.translate(dir.x, dir.y, dir.z);
	// Quick hack to get the camera to stay on the ground :
	core::vec4 pos = this->transform.get_translation_vec4();
	pos.y = this->heightmap_height;
	this->transform.set_translation(pos);
}

void entities::fps_camera::move_down(double scalar) {
	core::vec4 dir = this->compute_up_vector() *-scalar;
	this->transform.translate(dir.x, dir.y, dir.z);
	// Quick hack to get the camera to stay on the ground :
	core::vec4 pos = this->transform.get_translation_vec4();
	pos.y = this->heightmap_height;
	this->transform.set_translation(pos);
}

void entities::fps_camera::move_up(double scalar) {
	core::vec4 dir = this->compute_up_vector() *scalar;
	this->transform.translate(dir.x, dir.y, dir.z);
	// Quick hack to get the camera to stay on the ground :
	core::vec4 pos = this->transform.get_translation_vec4();
	pos.y = this->heightmap_height;
	this->transform.set_translation(pos);
}

core::vec4 entities::fps_camera::compute_heading_vector() const {
	return this->compute_forward_vector();
}

core::vec4 entities::fps_camera::compute_forward_vector() const {
	return this->transform.get_inverse() * core::vec4(1.,.0,.0,.0);
}

core::vec4 entities::fps_camera::compute_right_vector() const {
	return this->transform.get_inverse() * core::vec4(.0,.0,1.,.0);
}

core::vec4 entities::fps_camera::compute_up_vector() const {
	return this->transform.get_inverse() * core::vec4(.0,1.,.0,.0);
}
