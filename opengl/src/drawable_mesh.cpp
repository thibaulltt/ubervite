#include "../include/drawable_mesh.hpp"
#include "../../entities/include/generic_camera.hpp"

mesh::mesh(QOpenGLContext* context, const aiScene* scene, const aiMesh* mesh) : drawable(context) {
	// Setup the vertices needed :
	this->vertices.clear();
	this->normals.clear();
	this->uvs.clear();
	this->colors.clear();
	this->faces.clear();
	this->oobb = core::bounding_box();

	bool has_vertices = mesh->HasPositions();
	bool has_normals = mesh->HasNormals();
	bool has_faces = mesh->HasFaces();
	std::vector<uint> color_channels;
	std::vector<uint> uv_channels;
	for (unsigned int i = 0; i < mesh->GetNumColorChannels(); ++i) {
		if (mesh->HasVertexColors(i)) color_channels.push_back(i);
	}
	for (unsigned int i = 0; i < mesh->GetNumUVChannels(); ++i) {
		if (mesh->HasTextureCoords(i)) uv_channels.push_back(i);
	}

	// Iterate on the aiMesh's data :
	for (uint i = 0; i < mesh->mNumVertices; ++i) {
		if (has_vertices){
			aiVector3D pos = mesh->mVertices[i];
			core::vec4 p = core::vec4(static_cast<double>(pos[0]), static_cast<double>(pos[1]), static_cast<double>(pos[2]), 1.0);
			this->vertices.push_back(p);
			this->oobb.extend(p);
		}
		if (has_normals) {
			aiVector3D pos = mesh->mNormals[i];
			this->normals.push_back(core::vec4(static_cast<double>(pos[0]), static_cast<double>(pos[1]), static_cast<double>(pos[2]), 0.0));
		}
		if (color_channels.size() > 0) {
			aiColor4D blend_colors = aiColor4D();
			for (size_t j = 0; j < color_channels.size(); ++j) {
				blend_colors += mesh->mColors[color_channels[j]][i];
			}
			blend_colors /= static_cast<float>(color_channels.size());
			this->colors.push_back(core::vec4(blend_colors.r, blend_colors.g, blend_colors.b, 1.0));
		} else {
			this->colors.push_back(core::vec4(.217,.217,.217,1.));
		}
		if (uv_channels.size() > 0) {
			// aiVector
			// TODO : make it nicely play with aiMesh's data, and load UV coords
		}
	}

	this->bb = new drawable_bounding_box(context, this->oobb);

	if (has_faces) {
		for (uint i = 0; i < mesh->mNumFaces; ++i) {
			aiFace curFace = mesh->mFaces[i];
			std::vector<uint32_t> faceVec = std::vector<uint32_t>();
			assert(curFace.mNumIndices == 3);
			this->faces.push_back(static_cast<uint32_t>(curFace.mIndices[0]));
			this->faces.push_back(static_cast<uint32_t>(curFace.mIndices[1]));
			this->faces.push_back(static_cast<uint32_t>(curFace.mIndices[2]));
			faceVec.clear();
		}
	}
	std::cout << "\tMesh " << mesh->mName.C_Str() << " : " << std::endl;
	std::cout << "\t- " << this->vertices.size() << " vertices" << std::endl;
	std::cout << "\t- " << this->normals.size() << " normals" << std::endl;
	std::cout << "\t- " << this->faces.size() << " face indices" << std::endl;
}

void mesh::init_gl() {
	// Setup basic attributes :
	this->set_draw_mode(GL_TRIANGLES);
	this->is_hidden = false;
	this->elements_to_draw = static_cast<GLsizei>(this->faces.size());

	// Load shaders :
	this->load_shaders();

	// Create VAO, and bind it
	if (this->vao.create()) {
		QOpenGLVertexArrayObject::Binder vaoBinder(&this->vao);

		// Create vertex buffer
		if (this->vertex_buffer.create()) {
			if (this->vertex_buffer.bind()) {
				this->vertex_buffer.allocate(this->vertices.data(), static_cast<int>(this->vertices.size() * sizeof(core::vec4)));
				this->vertex_buffer.setUsagePattern(QOpenGLBuffer::StaticDraw);
				this->vertex_buffer.release();
			} else {
				std::cerr << "Vertex buffer could not be bound. No data was allocated." << std::endl;
			}
		} else {
			std::cerr << "Vertex buffer could not be created. No data was allocated." << std::endl;
		}

		// Create normal buffer
		if (this->normal_buffer.create()) {
			if (this->normal_buffer.bind()) {
				this->normal_buffer.allocate(this->normals.data(), static_cast<int>(this->normals.size() * sizeof(core::vec4)));
				this->normal_buffer.setUsagePattern(QOpenGLBuffer::StaticDraw);
				this->normal_buffer.release();
			} else {
				std::cerr << "Normal buffer could not be bound. No data was allocated." << std::endl;
			}
		} else {
			std::cerr << "Normal buffer could not be created. No data was allocated." << std::endl;
		}

		// Create color buffer
		if (this->color_buffer.create()) {
			if (this->color_buffer.bind()) {
				this->color_buffer.allocate(this->colors.data(), static_cast<int>(this->colors.size() * sizeof(core::vec4)));
				this->color_buffer.setUsagePattern(QOpenGLBuffer::StaticDraw);
				this->color_buffer.release();
			} else {
				std::cerr << "Color buffer could not be bound. No data was allocated." << std::endl;
			}
		} else {
			std::cerr << "Color buffer could not be created. No data was allocated." << std::endl;
		}

		// Create uv buffer
		if (this->uv_buffer.create()) {
			if (this->uv_buffer.bind()) {
				this->uv_buffer.allocate(this->uvs.data(), static_cast<int>(this->uvs.size() * sizeof(core::vec2)));
				this->uv_buffer.setUsagePattern(QOpenGLBuffer::StaticDraw);
				this->uv_buffer.release();
			} else {
				std::cerr << "UV buffer could not be bound. No data was allocated." << std::endl;
			}
		} else {
			std::cerr << "UV buffer could not be created. No data was allocated." << std::endl;
		}

		// Create face buffer
		if (this->face_buffer.create()) {
			if (this->face_buffer.bind()) {
				this->face_buffer.allocate(this->faces.data(), static_cast<int>(this->faces.size() * sizeof(uint32_t)));
				this->face_buffer.setUsagePattern(QOpenGLBuffer::StaticDraw);
				this->face_buffer.release();
			} else {
				std::cerr << "Face buffer could not be bound. No data was allocated." << std::endl;
			}
		} else {
			std::cerr << "Face buffer could not be created. No data was allocated." << std::endl;
		}

		this->setup_vertex_attribs();

		this->bb->init_gl();
	} else {
		std::cerr << "mesh : Could not create a VAO object. No OpenGL storage was allocated." << std::endl;
	}
}

void mesh::setup_vertex_attribs() {
	QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();

	this->vertex_buffer.bind();
	f->glEnableVertexAttribArray(0);
	f->glVertexAttribPointer(0, 4, GL_DOUBLE, GL_FALSE, 4*sizeof(double), nullptr);
	this->vertex_buffer.release();

	this->normal_buffer.bind();
	f->glEnableVertexAttribArray(1);
	f->glVertexAttribPointer(1, 4, GL_DOUBLE, GL_FALSE, 4*sizeof(double), nullptr);
	this->normal_buffer.release();

	this->color_buffer.bind();
	f->glEnableVertexAttribArray(2);
	f->glVertexAttribPointer(2, 4, GL_DOUBLE, GL_FALSE, 4*sizeof(double), nullptr);
	this->color_buffer.release();

	this->uv_buffer.bind();
	f->glEnableVertexAttribArray(3);
	f->glVertexAttribPointer(3, 2, GL_DOUBLE, GL_FALSE, 2*sizeof(double), nullptr);
	this->uv_buffer.release();
}

std::pair<core::vec4, double> mesh::check_raycast_intersection(const std::pair<core::vec4, core::vec4>& ray) const {
	// Get a value in case there's no intersection
	std::pair<core::vec4, double> no_intersection = std::make_pair(ray.first, std::numeric_limits<double>::max());

	// Intersection with a bounding box is defined as the existence of a point inbetween
	// three pairs of planes defined by the sides of the bounding box. As such :
	double dir_ray_x = 1.0 / ray.second.x;
	double dir_ray_y = 1.0 / ray.second.y;
	double dir_ray_z = 1.0 / ray.second.z;

	double t_min_x = (this->oobb.get_min().x - ray.first.x) * dir_ray_x;
	double t_max_x = (this->oobb.get_max().x - ray.first.x) * dir_ray_x;
	double t_min_y = (this->oobb.get_min().y - ray.first.y) * dir_ray_y;
	double t_max_y = (this->oobb.get_max().y - ray.first.y) * dir_ray_y;
	double t_min_z = (this->oobb.get_min().z - ray.first.z) * dir_ray_z;
	double t_max_z = (this->oobb.get_max().z - ray.first.z) * dir_ray_z;

	double t_min = std::max(std::max(std::min(t_min_x, t_max_x), std::min(t_min_y, t_max_y)), std::min(t_min_z, t_max_z));
	double t_max = std::min(std::min(std::max(t_min_x, t_max_x), std::max(t_min_y, t_max_y)), std::max(t_min_z, t_max_z));

	// The BB is behind the ray
	if (t_max < .0) {
		return no_intersection;
	}
	// There is no intersection
	if (t_min > t_max) {
		return no_intersection;
	}

	// There is an intersection ! Compute it :
	return std::make_pair(ray.first + ray.second * t_min, t_min);
}

void mesh::render_object(const core::mat4 model_matrix) {
	if (!QOpenGLContext::currentContext()->isValid()) {
		std::cerr << "Current opengl context is not valid" << std::endl;
	}
	if (!is_hidden) {
		// Bind the VAO object, to draw it :
		QOpenGLVertexArrayObject::Binder vaoBinder(&this->vao);
		if (!this->program->bind()) {
			std::cerr << "Program not bound, cannot draw." << std::endl;
			return;
		}

		int proj_loc = this->program->uniformLocation("projection_matrix");
		int view_loc = this->program->uniformLocation("view_matrix");
		int model_loc = this->program->uniformLocation("model_matrix");
		int light_loc = this->program->attributeLocation("light_position_worldspace");

		if (proj_loc > -1) { this->program->setUniformValue(proj_loc, QMatrix4x4(camera->get_projection_matrix().to_array_float())); } //
		if (view_loc > -1) { this->program->setUniformValue(view_loc, QMatrix4x4(camera->get_view_matrix().to_array_float())); }
		if (model_loc > -1) { this->program->setUniformValue(model_loc, QMatrix4x4(model_matrix.to_array_float())); }
		if (light_loc > -1) { this->program->setUniformValue(light_loc, QVector4D(-45.f, 5.f, -6.f,1.f)); }

		if (this->face_buffer.bind()) {
			QOpenGLContext::currentContext()->functions()->glDrawArrays(this->draw_mode, 0, this->elements_to_draw); //, GL_UNSIGNED_SHORT, nullptr);//glDrawElements
		}

		this->program->release();
	} else {
		if (this->bb != nullptr) {
			this->bb->render_object(model_matrix);
		}
	}
}

mesh::~mesh() {
	this->vertices.clear();
	this->normals.clear();
	this->colors.clear();
	this->uvs.clear();
	this->faces.clear();
}
