#ifndef _ENTITIES_INCLUDE_TRACKBALL_CAMERA_HPP_
#define _ENTITIES_INCLUDE_TRACKBALL_CAMERA_HPP_

#include "../../core/core_include_all.hpp"
#include "generic_camera.hpp"

// TODO : Make it move on X, Y, Z, relative to itself, not the world.

namespace entities {

	/**
	 * @brief The trackball_camera class represents a very simple camera implementation.
	 * @details It consists of a camera position, defined by the user when constructing
	 * it, and by two angles, defining a single "look-at" point on the surface of a unit
	 * sphere. This class will eventually replaced by a more sophisticated camera class
	 * somewhere along the game's development.
	 */
	class trackball_camera : public generic_camera {

		public:
			/**
			 * @brief Default constructor, places a camera at the specified position.
			 * @param position The position of the camera. Defaults to the origin.
			 */
			trackball_camera(core::vec4 position) : generic_camera(position) {}
			/**
			 * @brief Updates the view_matrix member variable according the trackball behaviour
			 */
			virtual void update_view_matrix() override;
	};
}

#endif // _ENTITIES_INCLUDE_TRACKBALL_CAMERA_HPP_
