/**==========================================
 * FILE : helpers/include/xml_types.hpp
 * AUTH : Thibault de VILLÈLE
 * DATE : 31/10/2019
 * DESC : Contient des types utiles pour la
 * 	  tokenisation d'un contenu XML pour
 * 	  la classe xml_parser définie dans
 * 	  le fichier xml.hpp.
 * ==========================================
 */

#ifndef _HELPERS_INCLUDE_XML_TYPES_HPP_
#define _HELPERS_INCLUDE_XML_TYPES_HPP_

#include <string>

namespace helpers {

	namespace xml {

		/* Définis à la main pour que les valeurs ne se
		superposent pas si l'on veut les vérifier par ET */
		typedef enum available_token_types {
			UNIDENTIFIED =		0b0000000000000000,
			TAG_END =		0b0000000000000001,
			TAG_SELF_CLOSING_END =	0b0000000000000010,
			TAG_OPEN =		0b0000000000000100,
			TAG_CONTENT =		0b0000000000001000,
			TAG_NAME =		0b0000000000010000,
			TAG_PROPERTY =		0b0000000000100000,
			TAG_PROP_VALUE =	0b0000000001000000,
			XML_DECLARATION_BEGIN = 0b0000000010000000,
			XML_DECLARATION_END =	0b0000000100000000,
			XML_DECLARATION_PROP =	0b0000001000000000,
			XML_DECLARATION_VALUE =	0b0000010000000000,
			RAW_CONTENT =		0b1000000000000000
		} token_t;

		/**
		 * Définit un "token", une des plus petites unités de traduction possible.
		 * Il sera utilisé notamment pour représenter l'ouverture d'une balise,
		 * la fermeture de celle-ci, ou une balise auto-fermante (qui se finit
		 * par />), ou bien encore une série de strings non identifiées, le nom
		 * d'une balise ... (etc)
		 *
		 * Est utilisé pour faire une analyse syntaxique très basique du XML d'une
		 * scène pour le moteur de jeu.
		 */
		struct token {
			/* Defines the type of token this is */
			token_t type;
			/* raw content of the token */
			std::string content;
		};

		token infer_token_type(const std::string&);

		token_t guess_tag_type(const std::string&);

	}

}

#endif // _HELPERS_INCLUDE_XML_TYPES_HPP_
