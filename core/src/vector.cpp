#include "../include/vector.hpp"

#include <cmath>

namespace core {

	/******************************************************
	************************* vec2 ************************
	******************************************************/

	vec2::vec2() : x(0.0), y(0.0) {}

	vec2::vec2(double _x, double _y) : x(_x), y(_y) {}

	vec2::vec2(const vec2& v) : x(v.x), y(v.y) {}

	vec2::vec2(vec2&& v) : x(v.x), y(v.y) {}

	double vec2::dot(const vec2& v) const {
		return this->x * v.x + this->y * v.y;
	}

	double vec2::abs_dot(const vec2& v) const {
		return std::abs(this->dot(v));
	}

	double vec2::length() const {
		return std::sqrt(this->squared_length());
	}

	double vec2::squared_length() const {
		return this->dot(*this);
	}

	vec2 vec2::normalize() const {
		double l = this->length();
		return vec2(this->x / l, this->y / l);
	}

	vec2& vec2::normalize_self() {
		double l = this->length();
		this->x = this->x / l;
		this->y = this->y / l;
		return *this;
	}

	double& vec2::operator[](int i) {
		return *(&this->x + i);
	}

	const double vec2::operator[](int i) const {
		return *(&this->x + i);
	}

	vec2 vec2::operator- () const {
		return vec2(-this->x, -this->y);
	}

	vec2& vec2::operator= (const vec2& v) {
		this->x = v.x;
		this->y = v.y;
		return *this;
	}

	vec2 vec2::operator+ (const vec2& v) const {
		return vec2(this->x + v.x, this->y + v.y);
	}

	vec2 vec2::operator- (const vec2& v) const {
		return vec2(this->x - v.x, this->y - v.y);
	}

	vec2 vec2::operator* (const double s) const {
		return vec2(this->x * s, this->y * s);
	}

	vec2 vec2::operator* (const vec2& v) const {
		return vec2(this->x * v.x, this->y * v.y);
	}

	vec2 vec2::operator/ (const double s) const {
		return *this * (1/s);
	}

	vec2& vec2::operator+=(const vec2& v) {
		this->x += v.x;
		this->y += v.y;
		return *this;
	}

	vec2& vec2::operator-=(const vec2& v) {
		this->x -= v.x;
		this->y -= v.y;
		return *this;
	}

	vec2& vec2::operator*=(const vec2& v) {
		this->x *= v.x;
		this->y *= v.y;
		return *this;
	}

	vec2& vec2::operator*=(const double s) {
		this->x *= s;
		this->y *= s;
		return *this;
	}

	vec2& vec2::operator/=(const double s) {
		this->x /= s;
		this->y /= s;
		return *this;
	}

	/******************************************************
	 ************************ vec3 ************************
	*****************************************************/

	vec3::vec3() : x(0.0), y(0.0), z(0.0) {}

	vec3::vec3(double _x, double _y, double _z) : x(_x), y(_y), z(_z) {}

	vec3::vec3(const vec3& v) : x(v.x), y(v.y), z(v.z) {}

	vec3::vec3(vec3&& v) : x(v.x), y(v.y), z(v.z) {}

	double vec3::dot(const vec3 &v) const {
		return this->x * v.x + this->y * v.y + this->z * v.z;
	}

	double vec3::abs_dot(const vec3 &v) const {
		return std::abs(this->dot(v));
	}

	double vec3::length() const {
		return std::sqrt(this->squared_length());
	}

	double vec3::squared_length() const {
		return this->dot(*this);
	}

	vec3 vec3::cross(const vec3 &v) const {
		double x = this->y * v.z - this->z * v.y;
		double y = this->z * v.x - this->x * v.z;
		double z = this->x * v.y - this->y * v.x;
		return vec3(x,y,z);
	}

	vec3 vec3::normalize() const {
		double l = this->length();
		return vec3(this->x / l, this->y / l, this->z / l);
	}

	vec3& vec3::normalize_self() {
		double l = this->length();
		this->x /= l;
		this->y /= l;
		this->z /= l;
		return *this;
	}

	double& vec3::operator[](int i) {
		return *(&this->x + i);
	}

	const double vec3::operator[](int i) const {
		return *(&this->x + i);
	}

	vec3 vec3::operator- () const {
		return vec3(-this->x, -this->y, -this->z);
	}

	vec3& vec3::operator= (const vec3& v) {
		this->x = v.x;
		this->y = v.y;
		this->z = v.z;
		return *this;
	}

	vec3 vec3::operator+ (const vec3& v) const {
		return vec3(this->x + v.x, this->y + v.y, this->z + v.z);
	}

	vec3 vec3::operator- (const vec3& v) const {
		return vec3(this->x - v.x, this->y - v.y, this->z - v.z);
	}

	vec3 vec3::operator* (const double s) const {
		return vec3(this->x * s, this->y * s, this->z * s);
	}

	vec3 vec3::operator* (const vec3& v) const {
		return vec3(this->x * v.x, this->y * v.y, this->z * v.z);
	}

	vec3 vec3::operator/ (const double s) const {
		return *this * (1/s);
	}

	vec3& vec3::operator+=(const vec3& v) {
		this->x += v.x;
		this->y += v.y;
		this->z += v.z;
		return *this;
	}

	vec3& vec3::operator-=(const vec3& v) {
		this->x -= v.x;
		this->y -= v.y;
		this->z -= v.z;
		return *this;
	}

	vec3& vec3::operator*=(const vec3& v) {
		this->x *= v.x;
		this->y *= v.y;
		this->z *= v.z;
		return *this;
	}

	vec3& vec3::operator*=(const double s) {
		this->x *= s;
		this->y *= s;
		this->z *= s;
		return *this;
	}

	vec3& vec3::operator/=(const double s) {
		this->x /= s;
		this->y /= s;
		this->z /= s;
		return *this;
	}

	/******************************************************
	 ************************ vec4 ************************
	*****************************************************/

	vec4::vec4() : x(0.0), y(0.0), z(0.0), w(0.0) {}

	vec4::vec4(double _x, double _y, double _z, double _w) : x(_x), y(_y), z(_z), w(_w) {}

	vec4::vec4(const vec4& v) : x(v.x), y(v.y), z(v.z), w(v.w) {}

	vec4::vec4(const vec3& v) : x(v.x), y(v.y), z(v.z), w(0.0) {}

	vec4::vec4(const vec2& v) : x(v.x), y(v.y), z(0.0), w(0.0) {}

	vec4::vec4(vec4&& v) : x(v.x), y(v.y), z(v.z), w(v.w) {}

	double vec4::dot(const vec4 &v) const {
		return this->x * v.x + this->y * v.y + this->z * v.z + this->w * v.w;
	}

	double vec4::abs_dot(const vec4 &v) const {
		return std::abs(this->dot(v));
	}

	vec4 vec4::cross(const vec4 &v) const {
		double ux = this->x;
		double vx = v.x;
		double uy = this->y;
		double vy = v.y;
		double uz = this->z;
		double vz = v.z;
		vec3 cross_p = vec3(ux,uy,uz).cross(vec3(vx,vy,vz));
		vec4 res = vec4(cross_p);
		res.w = this->w;
		return res;
	}

	double vec4::length() const {
		return std::sqrt(this->squared_length());
	}

	double vec4::squared_length() const {
		return this->dot(*this);
	}

	vec4 vec4::normalize() const {
		double l = this->length();
		return vec4(this->x / l, this->y / l, this->z / l, this->w / l);
	}

	vec4& vec4::normalize_self() {
		double l = this->length();
		this->x /= l;
		this->y /= l;
		this->z /= l;
		this->w /= l;
		return *this;
	}

	GLfloat* vec4::to_glfloat_array() const {
		GLfloat* res = new GLfloat[4];
		res[0] = static_cast<GLfloat>(this->operator[](0));
		res[1] = static_cast<GLfloat>(this->operator[](1));
		res[2] = static_cast<GLfloat>(this->operator[](2));
		res[3] = static_cast<GLfloat>(this->operator[](3));
		return res;
	}

	double& vec4::operator[](int i) {
		return *(&this->x + i);
	}

	const double vec4::operator[](int i) const {
		return *(&this->x + i);
	}

	vec4 vec4::operator- () const {
		return vec4(-this->x, -this->y, -this->z, -this->w);
	}

	vec4& vec4::operator= (const vec4& v) {
		this->x = v.x;
		this->y = v.y;
		this->z = v.z;
		this->w = v.w;
		return *this;
	}

	vec4 vec4::operator+ (const vec4& v) const {
		return vec4(this->x + v.x, this->y + v.y, this->z + v.z, this->w + v.w);
	}

	vec4 vec4::operator- (const vec4& v) const {
		return vec4(this->x - v.x, this->y - v.y, this->z - v.z, this->w - v.w);
	}

	vec4 vec4::operator* (const double s) const {
		return vec4(this->x * s, this->y * s, this->z * s, this->w * s);
	}

	vec4 vec4::operator* (const vec4& v) const {
		return vec4(this->x * v.x, this->y * v.y, this->z * v.z, this->w * v.w);
	}

	vec4 vec4::operator/ (const double s) const {
		return *this * (1/s);
	}

	vec4& vec4::operator+=(const vec4& v) {
		this->x += v.x;
		this->y += v.y;
		this->z += v.z;
		return *this;
	}

	vec4& vec4::operator-=(const vec4& v) {
		this->x -= v.x;
		this->y -= v.y;
		this->z -= v.z;
		return *this;
	}

	vec4& vec4::operator*=(const vec4& v) {
		this->x *= v.x;
		this->y *= v.y;
		this->z *= v.z;
		return *this;
	}

	vec4& vec4::operator*=(const double s) {
		this->x *= s;
		this->y *= s;
		this->z *= s;
		return *this;
	}

	vec4& vec4::operator/=(const double s) {
		this->x /= s;
		this->y /= s;
		this->z /= s;
		return *this;
	}

}
