#ifndef _CORE_INCLUDE_TRANSFORM_HPP_
#define _CORE_INCLUDE_TRANSFORM_HPP_

#include "vector.hpp"
#include "matrix.hpp"

namespace core {

	/**
	 * @brief Represents a transformation.
	 * @details The transform_t class is used to represent a transformation matrix in the program.
	 * Additionnaly, it also contains an inverse version of the current transformation matrix available on demand.
	 */
	class transform_t {
		protected:
			/**
			 * @brief The translation vector for the current transform.
			 */
			core::vec3 translation;
			/**
			 * @brief The rotation on the X, Y, and Z axes for the current transform.
			 */
			core::vec3 rotation;
			/**
			 * @brief The scaling factors on the X, Y, and Z axes.
			 */
			core::vec3 scaling;
		public:
			/**
			 * @brief Default constructor, builds a transform matrix at the origin.
			 */
			transform_t();
			/**
			* @brief Constructor initializing a transform matrix at the specified position
			* @param pos The position which should be set for the current transform
			*/
			transform_t(const core::vec4& pos);
			/**
			 * @brief Constructor initializing a transform at the specified position, rotation and scale as the matrix encodes.
			 * @param transform The transform matrix (from assimp)
			 */
			transform_t(const core::mat4& transform);
			/**
			 * @brief Copy constructor from a const transform
			 * @param t The transform to copy
			 */
			transform_t(const transform_t& t);
			/**
			 * @brief Copy constructor from a mutable transform
			 * @param t The transform to copy
			 */
			transform_t(transform_t&& t);
			/**
			 * @brief Default copy assignment operator
			 */
			transform_t& operator= (const transform_t& t);
			/**
			 * @brief Rotate along the X axis, by `angle` degrees.
			 * @param angle The amount of degrees to rotate the current matrix by.
			 * @return A reference to this (to chain together functions).
			 */
			transform_t& rotate_x(const double& angle);
			/**
			 * @brief Rotate along the Y axis, by `angle` degrees.
			 * @param angle The amount of degrees to rotate the current matrix by.
			 * @return A reference to this (to chain together functions).
			 */
			transform_t& rotate_y(const double& angle);
			/**
			 * @brief Rotate along the Z axis, by `angle` degrees.
			 * @param angle The amount of degrees to rotate the current matrix by.
			 * @return A reference to this (to chain together functions).
			 */
			transform_t& rotate_z(const double& angle);
			/**
			 * @brief Rotate the current transform.
			 * @param x The amount of degrees to rotate on X
			 * @param y The amount of degrees to rotate on Y
			 * @param z The amount of degrees to rotate on Z
			 * @return A reference to this (to chain together functions).
			 */
			transform_t& rotate(const double& x, const double& y, const double& z);
			/**
			 * @brief Rotate by the amount provided on the X, Y, and Z axes.
			 * @return A reference to this (to chain together functions).
			 */
			transform_t& rotate(const core::vec3&);
			/**
			 * @brief Translate along the X axis.
			 * @param x The distance to translate the current matrix by.
			 * @return A reference to this (to chain together functions).
			 */
			transform_t& translate_x(const double& x);
			/**
			 * @brief Translate along the Y axis.
			 * @param y The distance to translate the current matrix by.
			 * @return A reference to this (to chain together functions).
			 */
			transform_t& translate_y(const double& y);
			/**
			 * @brief Translate along the Z axis.
			 * @param z The distance to translate the current matrix by.
			 * @return A reference to this (to chain together functions).
			 */
			transform_t& translate_z(const double& z);
			/**
			 * @brief Translate along all axes at once.
			 * @param x The distance to translate the matrix by along X.
			 * @param y The distance to translate the matrix by along Y.
			 * @param z The distance to translate the matrix by along Z.
			 * @return A reference to this (to chain together functions).
			 */
			transform_t& translate(const double& x, const double& y, const double& z);
			/**
			 * @brief Translate along the vector provided.
			 * @param v The vector by which to translate the matrix.
			 * @return A reference to this (to chain together functions).
			 */
			transform_t& translate(const core::vec3& v);
			/**
			 * @brief Scale the X axis by `x`.
			 * @param x The amount of scaling to apply to the X axis.
			 * @return A reference to this (to chain together functions).
			 */
			transform_t& scale_x(const double& x);
			/**
			 * @brief Scale the Y axis by `y`.
			 * @param y The amount of scaling to apply to the Y axis.
			 * @return A reference to this (to chain together functions).
			 */
			transform_t& scale_y(const double& y);
			/**
			 * @brief Scale the Z axis by `z`.
			 * @param z The amount of scaling to apply to the Z axis.
			 * @return A reference to this (to chain together functions).
			 */
			transform_t& scale_z(const double& z);
			/**
			 * @brief Scale by `x` along X, by `y` along Y, and by `z` along Z.
			 * @param x The amount of scaling to apply to the X axis.
			 * @param y The amount of scaling to apply to the Y axis.
			 * @param z The amount of scaling to apply to the Z axis.
			 * @return A reference to this (to chain together functions).
			 */
			transform_t& scale(const double& x, const double& y, const double& z);
			/**
			 * @brief Scale according to the vector provided.
			 * @param v The vector by which to scale the matrix.
			 * @return A reference to this (to chain together functions).
			 */
			transform_t& scale(const core::vec3& v);
			/**
			 * @brief Gets the position matrix.
			 * @return The `position` attribute of the class.
			 */
			core::mat4 get_position() const;
			/**
			 * @brief Gets the position matrix, transposed for OpenGL.
			 * @return The `position` attribute of the class, transposed.
			 */
			core::mat4 get_position_opengl() const;
			/**
			 * @brief Gets the inverse matrix.
			 * @return The `inverse` attribute of the class.
			 */
			core::mat4 get_inverse() const;
			/**
			 * @brief Gets the inverse matrix, transposed for OpenGL.
			 * @return The `position` attribute of the class, transposed.
			 */
			core::mat4 get_inverse_opengl() const;
			/**
			 * @brief Get the rotation matrix of the current transform
			 * @return The rotation matrix of the current transform
			 */
			core::mat4 get_rotation_matrix() const;
			/**
			 * @brief Gets the current translation of the object
			 * @return The translation vector
			 */
			const core::vec3& get_translation() const;
			/**
			 * @brief Gets the current rotation of the object
			 * @return The rotation vector
			 */
			const core::vec3& get_rotation() const;
			/**
			 * @brief Gets the current scaling of the object
			 * @return The scaling vector
			 */
			const core::vec3& get_scaling() const;
			/**
			 * @brief Gets the current translation of the object
			 * @return The translation vector
			 */
			core::vec4 get_translation_vec4() const;
			/**
			 * @brief Gets the current rotation of the object
			 * @return The rotation vector
			 */
			core::vec4 get_rotation_vec4() const;
			/**
			 * @brief Gets the current scaling of the object
			 * @return The scaling vector
			 */
			core::vec4 get_scaling_vec4() const;
			/**
			 * @brief Sets the current translation of the object
			 */
			void set_translation(core::vec4);
			/**
			 * @brief Sets the current rotation of the object
			 */
			void set_rotation(core::vec4);
			/**
			 * @brief Sets the current scaling of the object
			 */
			void set_scaling(core::vec4);
	};
}

#endif // _CORE_INCLUDE_TRANSFORM_HPP_
