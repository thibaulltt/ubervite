#ifndef _QT_INCLUDE_INSPECTORS_HPP_
#define _QT_INCLUDE_INSPECTORS_HPP_

#include <QWidget>
//#include <QTreeWidgetItem>

#include "./input_fields.hpp"
#include "../../entities/include/generic_camera.hpp"
#include "../../scene/include/scene_object.hpp"

QT_FORWARD_DECLARE_CLASS(GLWidget)
QT_FORWARD_DECLARE_CLASS(camera_inspector)

class transform_inspector : public QWidget {
		Q_OBJECT
	protected:
		custom_vector_input* transform_input;
		custom_vector_input* rotation_input;
		custom_vector_input* scale_input;
		core::transform_t* ref;
	public:
		/**
		 * @brief Creates a transform inspector. Will link everything to create an interactive inspector.
		 * @param transform Transform to inspect and manipulate
		 * @param name Name of the transform's object
		 * @param parent Parent widget
		 */
		transform_inspector(core::transform_t* transform, std::string name = "untitled_gameobject", camera_inspector* cam_insp = nullptr);
		transform_inspector(core::transform_t* transform, std::string name = "untitled_gameobject", QWidget* parent = nullptr);
	public slots:
		void signal_passthrough();
		void update_internal_values();
	signals:
		void any_value_changed();
};

class camera_projection_inspector : public QWidget {
	Q_OBJECT
	protected:
		custom_float_input* left_input;
		custom_float_input* right_input;
		custom_float_input* bottom_input;
		custom_float_input* top_input;
		custom_float_input* near_input;
		custom_float_input* far_input;
		custom_checkbox_input* custom_matrix_choice;
		custom_checkbox_input* transpose_choice;
		custom_checkbox_input* info_debug_matrix_choice;
		proj_params* ref;
	public:
		camera_projection_inspector(camera_inspector* cam_insp);
	public slots:
		void signal_passthrough();
		void update_internal_values();
	signals:
		void any_value_changed();
};

class camera_info_inspector : public QWidget {
	Q_OBJECT
	protected:
		custom_float_input* camera_pos_x_input;
		custom_float_input* camera_pos_y_input;
		custom_float_input* camera_pos_z_input;
		custom_float_input* camera_rho_input;
		custom_float_input* camera_theta_input;
		custom_float_input* camera_radius_input;
		entities::generic_camera* ref;
	public:
		camera_info_inspector(camera_inspector* cam_insp);
	public slots:
		void signal_passthrough();
		void update_internal_values();
	signals:
		void any_value_changed();
};

class camera_inspector : public QWidget {
		Q_OBJECT
	protected:
		transform_inspector* camera_transform_input;
		camera_info_inspector* camera_info_input;
		camera_projection_inspector* projection_input;
		entities::generic_camera* ref;
	public:
		camera_inspector(entities::generic_camera* camera, GLWidget* glw);
	public slots:
		void signal_passthrough();
		void update_internal_values();
	signals:
		void any_value_changed();
		void glwidget_message_projection();
		void glwidget_message_view();
		void glwidget_message_transform();
};

#endif // _QT_INCLUDE_INSPECTORS_HPP_
