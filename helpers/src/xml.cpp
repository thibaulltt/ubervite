#include "../include/xml.hpp"

#include "../include/files.hpp"

#include <algorithm>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <ios>

/* Note : tous les en-têtes #ifndef sont là pour vérifier que nous sommes dans un environnement
 * de débogage, et seront ignorés lorsque le fichier sera compilé en mode Release. */

using namespace helpers;

/* Constructeur initialisant un flux d'entrée pour lecture de fichiers. {{{ */
xml::parser::parser(const std::string& path_name_for_stream) {
	file_avail_t file_availability = check_file_existence(path_name_for_stream);
	if (file_availability != file_avail_t::AVAILABLE) {
		std::cerr << "Ne peut pas ouvrir le fichier à l'emplacement " << path_name_for_stream << ". Veuillez " << std::endl
			<< "vérifier que le chemin d'accès est correct, et que le fichier n'est pas" << std::endl
			<< "en lecture seule.\nArrêt de l'analyseur de fichier sans avoir effectué d'actions." << std::endl;
		#ifndef NDEBUG
		std::cerr << "Reason for exiting early : " <<
			((file_availability == file_avail_t::READ_ONLY) ? "file is read-only." : "file does not exist." )
		<< std::endl;
		#endif
		throw std::ios_base::failure(((file_availability == file_avail_t::READ_ONLY) ?
			"Fichier "+path_name_for_stream+" est en lecture seule." :
			"Fichier "+path_name_for_stream+" n'existe pas." ));
	}
	// Le fichier est donc disponible
	this->file = std::ifstream(path_name_for_stream, std::ios::in);
	if (!this->file.good()) {
		std::cerr << "Le fichier n'a pas pu être ouvert correctement. Le bit de good-ness n'est pas activé." << std::endl;
		std::cerr << "Sortie, aucun fichier analysé." << std::endl;
		throw std::ios_base::failure(path_name_for_stream+" n'a pas été ouvert correctement.");
	}
	this->root_scope = nullptr;
}
/* }}} */

/* Changement de fichier source pour l'analyseur. {{{ */
void xml::parser::pick_file(const std::string& new_path_name) {
	file_avail_t file_availability = check_file_existence(new_path_name);
	if (file_availability != file_avail_t::AVAILABLE) {
		std::cerr << "Ne peut pas ouvrir le fichier à l'emplacement " << new_path_name << ". Veuillez " << std::endl
			<< "vérifier que le chemin d'accès est correct, et que le fichier n'est pas" << std::endl
			<< "en lecture seule.\nArrêt de helpers::xml::parser::pick_file() sans avoir effectué d'actions." << std::endl;
		#ifndef NDEBUG
		std::cerr << "Reason for exiting early : " <<
			((file_availability == file_avail_t::READ_ONLY) ? "file is read-only." : "file does not exist." )
		<< std::endl;
		#endif
		throw std::ios_base::failure(((file_availability == file_avail_t::READ_ONLY) ?
			"Fichier "+new_path_name+" est en lecture seule." :
			"Fichier "+new_path_name+" n'existe pas." ));
	}
	// Le fichier est donc disponible
	this->file = std::ifstream(new_path_name, std::ios::in);
	if (!this->file.good()) {
		std::cerr << "Le fichier n'a pas pu être ouvert correctement. Le bit de good-ness n'est pas activé." << std::endl;
		std::cerr << "Sortie, aucun fichier analysé." << std::endl;
		throw std::ios_base::failure(new_path_name+" n'a pas été ouvert correctement.");
	}
	#ifndef NDEBUG
	if (this->root_scope != nullptr) {
		std::cerr << "Ancienne hiérarchie va être effacée." << std::endl;
	}
	#endif
	this->root_scope = nullptr;
}
/* }}} */

/* Analyse rapide d'un fichier pour savoir les scènes disponibles dedans {{{ */

/* }}} */

