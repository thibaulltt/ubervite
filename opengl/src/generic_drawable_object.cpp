#include "../include/generic_drawable_object.hpp"

#include "../../helpers/include/files.hpp"
#include "../../entities/include/generic_camera.hpp"

drawable::drawable(QOpenGLContext* context, const std::string vpath, const std::string fpath, GLenum dm) : QOpenGLFunctions(context),
program(new QOpenGLShaderProgram(context)), vertex_buffer(QOpenGLBuffer::VertexBuffer), normal_buffer(QOpenGLBuffer::VertexBuffer),
uv_buffer(QOpenGLBuffer::VertexBuffer), face_buffer(QOpenGLBuffer::IndexBuffer), draw_mode(dm),
vertex_shader_path(vpath), fragment_shader_path(fpath), is_hidden(false) {
	this->projection_matrix_location = -1;
	this->view_matrix_location = -1;
	this->model_matrix_location = -1;
	this->light_position_worldspace_location = -1;
}

void drawable::init_gl() {
	/**
	 * In daughter classes :
	 *     - this->load_shaders()
	 *     - create and allocate VAO
	 *     - create and allocate each VBO
	 *     - call setup_vertex_attribs()
	 */
	return;
}

void drawable::set_vertex_shader_path(const std::string vpath) {
	this->vertex_shader_path = vpath;
	// Load shaders only if an underlying valid OpenGL context exists
	if (this->vao.isCreated()) {
		std::cout << "A valid OpenGL context was found.\nReloading vertex shader ..." << std::endl;
		this->load_shaders();
	}
}

void drawable::set_fragment_shader_path(const std::string fpath) {
	this->fragment_shader_path = fpath;
	// Load shaders only if an underlying valid OpenGL context exists
	if (this->vao.isCreated()) {
		std::cout << "A valid OpenGL context was found.\nReloading fragment shader ..." << std::endl;
		this->load_shaders();
	}
}

void drawable::set_draw_mode(GLenum dm) {
	this->draw_mode = dm;
}

void drawable::set_hidden(bool visibility) {
	this->is_hidden = visibility;
}

void drawable::toggle_hidden() {
	this->is_hidden = !this->is_hidden;
}

void drawable::setup_vertex_attribs() {
	// Will be implemented in child classes
	return;
}

void drawable::update_vertex_data() {
	// Will be implemented in child classes
	return;
}

std::pair<core::vec4, double> drawable::check_raycast_intersection(const std::pair<core::vec4, core::vec4>& ray) const {
	return std::make_pair(ray.first, std::numeric_limits<double>::max());
}

void drawable::render_object(const core::mat4 model_matrix) {
	if (!QOpenGLContext::currentContext()->isValid()) {
		std::cerr << "Current opengl context is not valid" << std::endl;
	}
	if (!is_hidden) {
		// Bind the VAO object, to draw it :
		QOpenGLVertexArrayObject::Binder vaoBinder(&this->vao);
		if (!this->program->bind()) {
			std::cerr << "Program not bound, cannot draw." << std::endl;
			return;
		}

		int proj_loc = this->program->uniformLocation("projection_matrix");
		int view_loc = this->program->uniformLocation("view_matrix");
		int model_loc = this->program->uniformLocation("model_matrix");
		int light_loc = this->program->attributeLocation("light_position_worldspace");

		if (proj_loc > -1) { this->program->setUniformValue(proj_loc, QMatrix4x4(camera->get_projection_matrix().to_array_float())); } //
		if (view_loc > -1) { this->program->setUniformValue(view_loc, QMatrix4x4(camera->get_view_matrix().to_array_float())); }
		if (model_loc > -1) { this->program->setUniformValue(model_loc, QMatrix4x4(model_matrix.to_array_float())); }
		if (light_loc > -1) { this->program->setUniformValue(light_loc, QVector4D(-45.f, 5.f, -6.f,1.f)); }

		if (this->face_buffer.bind()) {
			QOpenGLContext::currentContext()->functions()->glDrawArrays(this->draw_mode, 0, this->elements_to_draw); //, GL_UNSIGNED_SHORT, nullptr);//glDrawElements
		}

		this->program->release();
	} else {
		std::cerr << "Could not render object, it was hidden" << std::endl;
	}
}

drawable::~drawable() {
	this->color_buffer.destroy();
	this->face_buffer.destroy();
	this->uv_buffer.destroy();
	this->normal_buffer.destroy();
	this->vertex_buffer.destroy();
	this->vao.destroy();
	this->program->removeAllShaders();
}

void drawable::load_shaders() {
	std::string vertex_shader_code = helpers::file_to_string(this->vertex_shader_path);
	std::string fragment_shader_code = helpers::file_to_string(this->fragment_shader_path);

	this->program = new QOpenGLShaderProgram;
	// Compile vertex shader and check for errors
	if (this->program->addShaderFromSourceFile(QOpenGLShader::Vertex, QString::fromUtf8(this->vertex_shader_path.c_str())) == false)  {
		std::cerr << "Error while compiling vertex shaders. Log : " << std::endl;
		std::cerr << this->program->log().toStdString() << std::endl;
		std::cerr << "======= END VERTEX SHADER LOG =======" << std::endl;
	}

	// Compile fragment shader and check for errors
	if (this->program->addShaderFromSourceFile(QOpenGLShader::Fragment, QString::fromUtf8(this->fragment_shader_path.c_str())) == false) {
		std::cerr << "Error while compiling fragment shaders. Log : " << std::endl;
		std::cerr << this->program->log().toStdString() << std::endl;
		std::cerr << "======= END FRAGMENT SHADER LOG =======" << std::endl;
	}

	// Bind "location(layout=n)" attributes in the shaders
	this->program->bindAttributeLocation("position_modelspace", 0);
	this->program->bindAttributeLocation("normal_modelspace", 1);
	this->program->bindAttributeLocation("vertex_color", 2);
	this->program->bindAttributeLocation("vertex_uv", 3);

	// Try to link the program
	if (this->program->link() == false) {
		std::cerr << "Error while linking the shaders. Log : " << std::endl;
		std::cerr << this->program->log().toStdString() << std::endl;
		std::cerr << "======= END SHADER LOG =======" << std::endl;
	}

	if (this->program->bind()) {
		this->projection_matrix_location = this->program->uniformLocation("projection_matrix");
		this->view_matrix_location = this->program->uniformLocation("view_matrix");
		this->model_matrix_location = this->program->uniformLocation("model_matrix");
		this->light_position_worldspace_location = this->program->uniformLocation("light_position_worldspace");
	}
}
