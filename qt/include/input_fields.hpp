#ifndef _QT_INCLUDE_INPUT_FIELDS_HPP_
#define _QT_INCLUDE_INPUT_FIELDS_HPP_

#include <iostream>
#include <array>

#include <QWidget>
#include <QDoubleSpinBox>
#include <QCheckBox>
#include <QLabel>

#include "../../core/core_include_all.hpp"

class float_input_box : public QDoubleSpinBox {
	public:
		float_input_box(double current_val = .0, QWidget* parent = nullptr) : QDoubleSpinBox(parent) {
			this->setMinimum(-1000.); // Min value
			this->setMaximum(1000.0); // Max value
			this->setWrapping(false); // Don't wrap around
			this->setSingleStep(.01); // Step increment when clicking or left/right kb input
			this->setDecimals(2); // 4 decimal places
			this->setValue(current_val);
		}
};

class custom_float_input : public QWidget {
		Q_OBJECT
	protected:
		float_input_box* input;
	public:
		custom_float_input(std::string name, const double* value, QWidget* parent = nullptr);
		/**
		 * @brief When the field needs to be updated.
		 */
		void update_field(double);
		double get_value() { return this->input->value(); }
	public slots:
		/**
		 * @brief Used to send the signal from the input box to the parent element.
		 */
		void signal_passthrough();
	signals:
		/**
		 * @brief Signals when the value in the QDoubleSpinBox has changed
		 */
		void value_changed();
};

class custom_checkbox_input : public QWidget {
		Q_OBJECT
	protected:
		QCheckBox* checkbox;
	public:
		custom_checkbox_input(std::string name, bool* value, QWidget* parent = nullptr);
		/**
		 * @brief When the field needs to be updated.
		 */
		void update_field(bool);
		bool get_value() { return this->checkbox->checkState() == Qt::CheckState::Checked; }
	public slots:
		/**
		 * @brief Used to send the signal from the input box to the parent element.
		 */
		void signal_passthrough();
	signals:
		/**
		 * @brief Signals when the value in the QDoubleSpinBox has changed
		 */
		void value_changed();
};

class custom_vector_input : public QWidget {
		Q_OBJECT
	protected:
		float_input_box* x_input;
		float_input_box* y_input;
		float_input_box* z_input;
		float_input_box* w_input;
	public:
		custom_vector_input(core::vec4 values, std::string name = "vector", QWidget* parent = nullptr);
		void update_field(core::vec4);
		core::vec4 get_value();
	public slots:
		void signal_passthrough();
	signals:
		void value_changed();
};

#endif // _QT_INCLUDE_INPUT_FIELDS_HPP_
