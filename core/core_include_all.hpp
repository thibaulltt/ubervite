#ifndef _CORE_CORE_INCLUDE_ALL_HPP_
#define _CORE_CORE_INCLUDE_ALL_HPP_

#include "include/core.hpp"
#include "include/vector.hpp"
#include "include/matrix.hpp"
#include "include/transform.hpp"
#include "include/bounding_box.hpp"

#endif // _CORE_CORE_INCLUDE_ALL_HPP_
