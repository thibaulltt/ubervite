# A propos de _Ubervite_

_Ubervite_ est un jeu réalisé dans le cadre de l'UE HMIN317 - Moteurs de jeux pour le projet. C'est un jeu en
première personne où le but est de résoudre certaines énigmes.

### Liste des fonctionnalités (TODO)

- Scène
	- [x] Charger une scène (utilisant `assimp`)
	- [x] Charger un objet depuis le système de fichiers utilisateur (même plusieurs)
	- [~] Afficher une scène dans un contexte _OpenGL_
	- [ ] Avoir un inspecteur de scène (pour débugage, et modélisation de niveau)
	- [ ] Implémenter des bounding boxes pour tous objets, et les afficher ou non selon le besoin
	- [ ] Implémenter une façon pour que des objets puissent avoir une action lorsque l'on "appuie" sur eux

- Contrôle
	- [x] Avoir une caméra qui bouge dans la scène
	- [x] Avoir une matrice de projection fonctionnelle
	- [ ] Voir doc GLM pour les fonctionnalités de projection de caméra
	- [ ] Intégrer une HAL pour la gestion clavier
	- [ ] Implémenter une méthode de raycast pour l'interactivité utilisateur

- [ ] Projet
	- [ ] Implémenter un moteur de collision très simpliste :
		- [ ] Détection de bounding box
		- [ ] Arrêt lorsque deux se touchent
	- [ ] Implémenter le niveau 1 (très simple, juste un bouton a activer)
