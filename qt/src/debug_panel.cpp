#include "../include/debug_panel.hpp"

#include "../include/glwidget.h"

#include <QMenuBar>
#include <QApplication>
#include <QCloseEvent>

debug_panel::debug_panel(GLWidget* glw) {
	this->main_screen = glw;
	this->camera_insp = new camera_inspector(camera, glw);
	this->addTab(this->camera_insp, "Camera");
	this->setParent(nullptr);
	connect(glw, &GLWidget::destroyed, this, &QWidget::close);
	this->setAttribute(Qt::WA_DeleteOnClose, true);
}

void debug_panel::closeEvent(QCloseEvent* event) {
	emit this->destroyed(this);
	event->accept();
}
